(defpackage file-map
  (:use :cl)
  (:export
   :create-file-mapping
   :open-file-mapping
   :copy-memory
   :map-view-of-file
   :unmap-view-of-file
   :close-handle))
(in-package :file-map)

(defun create-file-mapping (name buf-size)
  (ffi:clines "#include <windows.h>"
              "#include <stdio.h>"
              "#include <conio.h>"
              "#include <tchar.h>")
  (ffi:c-inline (name buf-size) (:cstring :unsigned-int) :pointer-void
                "{HANDLE hMapFile;
                 hMapFile = CreateFileMapping(
                               INVALID_HANDLE_VALUE,
                               NULL,
                               PAGE_READWRITE,
                               0,
                               #1,
                               TEXT(#0));
                  @(return)=hMapFile;}" :one-liner nil))

(defun open-file-mapping (name)
  (ffi:clines "#include <windows.h>"
              "#include <stdio.h>"
              "#include <conio.h>"
              "#include <tchar.h>"
              "#pragma comment(lib, \"user32.lib\")")
  (ffi:c-inline (name) (:cstring) :pointer-void
                "{HANDLE hMapFile;
                 hMapFile = OpenFileMapping(
                               FILE_MAP_ALL_ACCESS,
                               FALSE,
                               TEXT(#0));
                  @(return)=hMapFile;}" :one-liner nil))

(defun copy-memory (p-buf message)
  (ffi:clines "#include <windows.h>"
              "#include <stdio.h>"
              "#include <conio.h>"
              "#include <tchar.h>")
  (ffi:c-inline (p-buf message) (:pointer-void :cstring) :bool
                "CopyMemory((PVOID)#0, #1, (_tcslen(#1) * sizeof(TCHAR)))" :one-liner t))


(defun map-view-of-file (handle buf-size)
  (ffi:clines "#include <windows.h>"
              "#include <stdio.h>"
              "#include <conio.h>"
              "#include <tchar.h>")
  (ffi:c-inline (handle buf-size) (:pointer-void :unsigned-int) :pointer-void
                "{LPCTSTR pBuf;
                 
                 pBuf = (LPTSTR) MapViewOfFile(
                                   (HANDLE)#0,
                                   FILE_MAP_ALL_ACCESS,
                                   0,
                                   0,
                                   #1);
                  @(return)=pBuf;}" :one-liner nil))


(defun unmap-view-of-file (p-buf)
  (ffi:clines "#include <windows.h>"
              "#include <stdio.h>"
              "#include <conio.h>"
              "#include <tchar.h>")
  (ffi:c-inline (p-buf) (:pointer-void) :bool
                "UnmapViewOfFile(#0)" :one-liner t))


(defun close-handle (handle)
  (ffi:clines "#include <windows.h>"
              "#include <stdio.h>"
              "#include <conio.h>"
              "#include <tchar.h>")
  (ffi:c-inline (handle) (:pointer-void) :bool
                "CloseHandle((HANDLE)#0)" :one-liner t))


;; (defparameter *p-handle* (create-file-mapping "fake" 256))
;; (defparameter *p-buf* (map-view-of-file *p-handle* 256))

;; (defparameter *p-handle2* (open-file-mapping "fake"))
;; (defparameter *p-buf2* (map-view-of-file *p-handle2* 256))

;; *p-handle*
;; *p-buf*

;; *p-handle2*
;; *p-buf2*

;; (ffi:deref-array *p-buf2* '(:array :unsigned-char) 0)
                                                                
;; (setf (ffi:deref-array *p-buf* '(:array :unsigned-byte) 0) 12)
                                                                
;; (unmap-view-of-file *p-buf*)
;; (unmap-view-of-file *p-buf2*)
                                                                
;; (close-handle *p-handle*)
;; (close-handle *p-handle2*)

;; (copy-memory *p-buf* "just a test")

;; (setf (ffi:deref-array *p-buf* '(:array :float) 0) -5)
;; (setf (ffi:deref-array *p-buf* '(:array :float) 1) -5)
;; (setf (ffi:deref-array *p-buf* '(:array :float) 2) -5)

;; (loop :for i :from 0 to 2 :do (progn (princ (ffi:deref-array *p-buf* '(:array :float) i)) (terpri)))



