(in-package :crash)

(defun scan-for-end-of-string (from bytemap)
  (let ((string-end nil)
	(dist-from-from 0))
    (loop :while (and (null string-end) (< (+ dist-from-from from) repl:*mem-size*))
	  :do (let ((real-pos (+ dist-from-from from)))
		(if (= (ffi:deref-array bytemap '(array :uint32-t) real-pos) 12)
		    (setf string-end t)
		    (setf dist-from-from (+ dist-from-from 1)))))
    dist-from-from))

(defun get-last-index (mem-map from)
  (let ((skip 0)
	(skip-char 0))
    (loop :named find-index
	  :for i
	  :from from
	  :do (progn
		(if (and (= (ffi:deref-array mem-map '(:array :uint32-t) i) 0)
			 (= (ffi:deref-array mem-map '(:array :uint32-t) (+ i 1)) 0)
			 (= skip 0)
			 (= skip-char 0))
			 (return-from find-index i))
		(if (> skip 0)
			 (progn
			   (setf skip (- skip 1)))
			 (progn
			   (if (> skip-char 0)
			       (progn
				 (setf skip-char (- skip-char 1)))
			       (progn
				 (if (equal (operations:reverse-op (ffi:deref-array mem-map '(:array :uint32-t) i)) :push-string)
				     (progn
				       (setf skip-char (scan-for-end-of-string i mem-map)))
				     (progn
				       (setf skip (operations:num-post-params (operations:reverse-op (ffi:deref-array mem-map '(:array :uint32-t) i))))))))))))))



(defun compile-bytecode (file-name)
  (with-open-file (file-out (concatenate 'string file-name ".fasb") :direction :output :element-type '(unsigned-byte 8) :if-exists :supersede)
    (loop :for i
	  :from 0 to (- (* 4 (get-last-index repl:*prelude-map* 0)) 4)
	  :do (let ((byte-code (ffi:deref-array repl:*prelude-map* '(:array :unsigned-byte) i)))
		(write-byte byte-code file-out))))
  (with-open-file (file-out (concatenate 'string file-name ".fasb") :direction :output :element-type '(unsigned-byte 8) :if-exists :append)
    (loop :for i
	  :from 1 to (* 4 (get-last-index repl:*map* 1))
	  :do (let ((byte-code (ffi:deref-array repl:*map* '(:array :unsigned-byte) i)))
		(write-byte byte-code file-out)))))

;;(compile-bytecode "crash")

;; (decode-prelude 10)
;; (terpri)
