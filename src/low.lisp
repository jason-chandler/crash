(defpackage low
  (:use :cl :file-map :operations :repl))

(in-package low)

;; (op :end-input)

#|

 (set-up-repl)
 (defparameter *box* (make-instance 'godot-object :cs "BOX"))
 (defparameter *rotate-y* (make-instance 'godot-fun :parent *box* :cs "RotateY"))
 (defparameter *move-and-slide* (make-instance 'godot-fun :parent *box* :cs "MoveAndSlide"))
 (defparameter *ui-up* (make-instance 'godot-const :cs "ui_up"))
 (defparameter *ui-right* (make-instance 'godot-const :cs "ui_right"))
(class-of 3.0)
(push-to-stack 3.0)
 (clear :all)
 (clear-model)
 (clear-vm)
 
 (repl:print-uints 0 5)
 (repl:print-uints 0 12 t)
 (repl:clear :all)

 (add-to-prelude *box*)
 (add-to-prelude *rotate-y*)
 (add-to-prelude *move-and-slide*)
 (add-to-prelude *ui-up*)
 (add-to-prelude *ui-right*)
 (setf (arg-list *rotate-y*) '(60.5))
 (push-to-stack *rotate-y* nil t)
 (setf (cs *box*) "BOX")
 (setf (uint-id *box*) 0)
 (send-uints)

(uint-id *box*)

(push-to-stack *box*)

(op :end-string)
(op :push-string)
(push-bool t)
(push-to-stack t)
(push-to-stack 0.785398)
(push-to-stack 4)
(push-bool nil)
(push-to-stack nil)
(make-instance 'vec3 :x 0.0 :y 1.0 :z 0.0)
(make-instance 'vec3 :x 3.0 :y 16.0 :z 3.0)

(setf (arg-list *move-and-slide*) `(t 0.785398 4 nil ,(make-instance 'vec3 :x 0.0 :y 1.0 :z 0.0) ,(make-instance 'vec3 :x 0.0 :y 10.0 :z 0.0)))
(push-to-stack *move-and-slide*)



(with-process 
    (push-to-stack *rotate-y* nil t))

(with-prelude (*box* *rotate-y* *move-and-slide* *ui-up* *ui-right*)
(with-input-press
    *ui-up*
    (setf (arg-list *rotate-y*) '(5.5))
    (push-to-stack *rotate-y* nil t))

(with-input-press
    *ui-right*
    (setf (arg-list *rotate-y*) '(-5.5))
    (push-to-stack *rotate-y* nil t)))

|#
