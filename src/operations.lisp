(defpackage :operations
  (:use :cl)
  (:export op reverse-op num-post-params))

(in-package :operations)

(defvar *op-table* (make-hash-table))

(loop :for i :from 0
      :for x :in
      '(:RET ;; 0 
        :T ;; 1
        :NIL ;; 2
        :CLEAR ;; 3
        :PUSH-IMMEDIATE ;; 4
        :CONST-STR ;; 5
        :PUSH-FLOAT ;; 6
        :PUSH-DELTA-FLOAT ;; 7
        :PUSH-INT32 ;; 8
        :OBJ ;; 9
        :FUN ;; 10
        :PUSH-STRING ;; 11
        :END-STRING ;; 12
        :POP ;; 13
        :REG-OBJ ;; 14
        :REG-FUN ;; 15
        :REG-CONST-STR ;; 16
        :ADD ;; 17
        :SUB ;; 18
        :MUL ;; 19
        :DIV ;; 20
        :MOD ;; 21
        :VEC ;; 22
        :VADD ;; 23
        :VSUB ;; 24
        :VCROSS ;; 25
        :VDOT ;; 26
        :VMUL-SC ;; 27
        :VDIV-SC ;; 28
        :GET ;; 29
        :SET ;; 30
        :EQ ;; 31
        :NE ;; 32
        :LESS ;; 33
        :LE ;; 34
        :GE ;; 35
        :GREAT ;; 36
        :ELSE ;; 37
        :ENDIF ;; 38
        :PROC ;; 39
        :PHYS-PROC ;; 40
        :END-PROC ;; 41
        :INPUT-PRESS ;; 42
        :INPUT-JUST ;; 43
        :END-INPUT ;; 44
	:INVOKE ;; 45
	:INPUT-GET-MOUSEMODE ;; 46
        :INPUT-SET-MOUSEMODE ;; 47
        :PUSH-INPUT-EVENT ;; 48
        :PUSH-IE-MOUSEMOTION ;; 49
        :EVENT ;; 50
        :END-EVENT ;; 51
	:TYPE ;; 52
	:SET-LOCAL ;; 53
	:GET-LOCAL ;; 54
	:ELSE-INPUT ;; 55
	:ARRAY ;; 56
	:NEW ;; 57
	:META ;; 58
	:PUSH-RETURN ;; 59
	:PUSH-NULL ;; 60
	:IS-NULL) ;; 61
      :do (setf (gethash x *op-table*) i))

(defun op (key)
  (gethash key *op-table*))

(defun reverse-op (val)
  (let ((matching-key nil))
    (loop for key being the hash-keys of *op-table*
	  :do (if (= (op key) val)
		  (setf matching-key key)))
    matching-key))

(defun num-post-params (key)
  (let ((post-plist '(:RET 0
		      :T 0
		      :NIL 0
		      :CLEAR 0
		      :PUSH-IMMEDIATE 1
		      :CONST-STR 0
		      :PUSH-FLOAT 1
		      :PUSH-DELTA-FLOAT 1
		      :PUSH-INT32 1
		      :OBJ 0
		      :FUN 0
		      :PUSH-STRING 0
		      :END-STRING 0
		      :POP 0
		      :REG-OBJ 0
		      :REG-FUN 0
		      :REG-CONST-STR 0
		      :ADD 0
		      :SUB 0
		      :MUL 0
		      :DIV 0
		      :MOD 0
		      :VEC 0
		      :VADD 0
		      :VSUB 0
		      :VCROSS 0
		      :VDOT 0
		      :VMUL-SC 0
		      :VDIV-SC 0
		      :GET 0
		      :SET 0
		      :EQ 0
		      :NE 0
		      :LESS 0
		      :LE 0
		      :GE 0
		      :GREAT 0
		      :ELSE 0
		      :ENDIF 0
		      :PROC 0
		      :PHYS-PROC 0
		      :END-PROC 0
		      :INPUT-PRESS 0
		      :INPUT-JUST 0
		      :END-INPUT 0
		      :INVOKE 0
		      :INPUT-GET-MOUSEMODE 0
		      :INPUT-SET-MOUSEMODE 0
		      :PUSH-INPUT-EVENT 0
		      :PUSH-IE-MOUSEMOTION 0
		      :EVENT 0
		      :END-EVENT 0
		      :TYPE 0
		      :SET-LOCAL 0
		      :GET-LOCAL 0
		      :ELSE-INPUT 0
		      :ARRAY 0
		      :NEW 0
		      :META 0
		      :PUSH-RETURN 0
		      :PUSH-NULL 0
		      :IS-NULL 0)))
    (getf post-plist
	 key)))

;;(in-package :operations)

