(defpackage repl
  (:use :cl :file-map)
  (:export set-up-repl
	   close-repl
	   insert-uint
	   send-uints
	   print-uints
	   clear
	   decode
	   decode-prelude
	   *map*
	   *prelude-map*
	   *mem-size*
	   *mem-size-in-uints*))

(in-package repl)

(defvar *mem-size* 20480)
(defvar *mem-size-in-uints* (/ *mem-size* 4))
(defvar *ip* 1)
(defvar *prelude-ip* 0)

(defvar *handle* nil)
(defvar *prelude-handle* nil)
(defvar *map* nil)
(defvar *prelude-map* nil)

(defun set-up-repl ()
  (setf *handle* (file-map:create-file-mapping "MainLispMappingObject" *mem-size*))
  (setf *prelude-handle* (file-map:create-file-mapping "PreludeLispMappingObject" *mem-size*))
  (setf *map* (file-map:map-view-of-file *handle* *mem-size*))
  (setf *prelude-map* (file-map:map-view-of-file *prelude-handle* *mem-size*)))

(defun close-repl ()
  (file-map:unmap-view-of-file *map*)
  (file-map:unmap-view-of-file *prelude-map*)
  (file-map:close-handle *handle*)
  (file-map:close-handle *prelude-handle*))

(defun clear-uints ()
  (loop :for i
	:from 0 :to (- *mem-size-in-uints* 1)
	:do (setf (ffi:deref-array *map* '(:array :uint32-t) i) 0))
  (setf *ip* 1))

(defun clear-prelude-uints ()
  (loop :for i
	:from 0 :to (- *mem-size-in-uints* 1)
	:do (setf (ffi:deref-array *prelude-map* '(:array :uint32-t) i) 0))
  (setf *prelude-ip* 0))

(defun clear (key)
  (case key
    (:main (clear-uints))
    (:prelude (clear-prelude-uints))
    (:all (progn (clear-prelude-uints)
		 (clear-uints)))))

(defun insert-uint (uint &optional prelude aux)
  (if prelude
      (progn (setf (ffi:deref-array *prelude-map* '(:array :uint32-t) *prelude-ip*) uint)
	     (incf *prelude-ip*))
      (progn (setf (ffi:deref-array *map* '(:array :uint32-t) *ip*) uint)
	     (incf *ip*))))

(defun send-uints ()
  (setf (ffi:deref-array *map* '(:array :uint32-t) 0) 1))

(defun print-uints-from (begin end from-array)
  (progn (terpri)
	 (loop :for i
	       :from begin to end
	       :do (progn (princ (ffi:deref-array from-array '(:array :uint32-t) i))
			  (princ "  ")))))

(defun print-uints (begin end &optional prelude aux)
  (if prelude
      (print-uints-from begin end *prelude-map*)
      (print-uints-from begin end *map*)))

(defun scan-for-string-end (from bytemap)
  (let ((string-end nil)
	(dist-from-from 0))
    (loop :while (and (null string-end) (< (+ dist-from-from from) *mem-size*))
	  :do (let ((real-pos (+ dist-from-from from)))
		(if (= (ffi:deref-array bytemap '(array :uint32-t) real-pos) 12)
		    (setf string-end t)
		    (setf dist-from-from (+ dist-from-from 1)))))
    dist-from-from))

(defun decode (end)
  (let ((skip 0)
	(skip-char 0))
    (terpri)
    (loop :for i
	  :from 1 to end
	  :do (if (> skip 0)
		  (progn
		    (princ (ffi:deref-array *map* '(:array :uint32-t) i))
		    (princ "  ")
		    (setf skip (- skip 1)))
		  (progn
		    (if (> skip-char 0)
			(progn
			  (princ (code-char (ffi:deref-array *map* '(:array :uint32-t) i)))
			  (setf skip-char (- skip-char 1))
			  (when (= skip-char 0)
			      (princ "  ")))
			(progn
			  (if (equal (operations:reverse-op (ffi:deref-array *map* '(:array :uint32-t) i)) :push-string)
			      (progn
				(setf skip-char (scan-for-string-end i *map*))
				(princ (operations:reverse-op (ffi:deref-array *map* '(:array :uint32-t) i)))
				(princ "  "))
			      (progn
				(setf skip (operations:num-post-params (operations:reverse-op (ffi:deref-array *map* '(:array :uint32-t) i))))
				(princ (operations:reverse-op (ffi:deref-array *map* '(:array :uint32-t) i)))
				(princ "  "))))))))))

(defun decode-prelude (end)
  (let ((skip 0)
	(skip-char 0))
    (terpri)
    (loop :for i
	  :from 0 to end
	  :do (if (> skip 0)
		  (progn
		    (princ (ffi:deref-array *prelude-map* '(:array :uint32-t) i))
		    (princ "  ")
		    (setf skip (- skip 1)))
		  (progn
		    (if (> skip-char 0)
			(progn
			  (princ (code-char (ffi:deref-array *prelude-map* '(:array :uint32-t) i)))
			  (setf skip-char (- skip-char 1))
			  (when (= skip-char 0)
			      (princ "  ")))
			(progn
			  (if (equal (operations:reverse-op (ffi:deref-array *prelude-map* '(:array :uint32-t) i)) :push-string)
			      (progn
				(setf skip-char (scan-for-string-end i *prelude-map*))
				(princ (operations:reverse-op (ffi:deref-array *prelude-map* '(:array :uint32-t) i)))
				(princ "  "))
			      (progn
				(setf skip (operations:num-post-params (operations:reverse-op (ffi:deref-array *prelude-map* '(:array :uint32-t) i))))
				(princ (operations:reverse-op (ffi:deref-array *prelude-map* '(:array :uint32-t) i)))
				(princ "  "))))))))))
;; (operations:reverse-op 42)
;; (code-char 57)
;; (char-code #\!)
;; (operations:op :ne)
;; (in-package repl)
;; (set-up-repl)
;; (close-repl)
;; (insert-uint (operations:op :endif) t)
;; (repl:print-uints 0 68)
;; (print-uints 0 5 t)
;; (decode-prelude 400)
;; (decode  9)
;; (clear :all)
