(defpackage crash
  (:use :cl :model :operations :repl :high))
(in-package :crash)

(set-up-repl)

;; starter-objects
(progn
 (defparameter *spatial* (make-instance 'godot-object :cs "SPATIAL"))
 (defparameter *box* (make-instance 'godot-object :cs "BOX"))
 (defparameter *sphere* (make-instance 'godot-object :cs "SPHERE"))
 (defparameter *camera* (make-instance 'godot-object :cs "BOX/ROTATION/CAMERA"))
 (defparameter *player-rot* (make-instance 'godot-object :cs "BOX/ROTATION")))

;; constant-strings
(progn
  (defparameter *empty-vec3* (make-instance 'vec3 :x 0.0 :y 0.0 :z 0.0))
  (defparameter *lisp-vm* (make-instance 'godot-const :cs "Crash.LispVM"))
  (defparameter *ui-up* (make-instance 'godot-const :cs "ui_up"))
  (defparameter *ui-right* (make-instance 'godot-const :cs "ui_right"))
  (defparameter *ui-left* (make-instance 'godot-const :cs "ui_left"))
  (defparameter *ui-down* (make-instance 'godot-const :cs "ui_down"))
  (defparameter *ui-cancel* (make-instance 'godot-const :cs "ui_cancel"))
  (defparameter *ui-reset* (make-instance 'godot-const :cs "ui_reset"))
  (defparameter *ui-accept* (make-instance 'godot-const :cs "ui_accept"))
  (defparameter *ui-select* (make-instance 'godot-const :cs "ui_select"))
  (defparameter global-transform (make-instance 'godot-const :cs "GlobalTransform"))
  (defparameter basis (make-instance 'godot-const :cs "basis"))
  (defparameter gd-vec2 (make-instance 'godot-const :cs "Godot.Vector2, GodotSharp"))
  (defparameter gd-vec2-s (make-instance 'godot-const :cs "S_Godot.Vector2, GodotSharp"))
  (defparameter gd-vec3 (make-instance 'godot-const :cs "Godot.Vector3, GodotSharp"))
  (defparameter gd-vec3-s (make-instance 'godot-const :cs "S_Godot.Vector3, GodotSharp"))
  (defparameter relative (make-instance 'godot-const :cs "Relative"))
  (defparameter rotation-degrees (make-instance 'godot-const :cs "RotationDegrees"))
  (defparameter system-single (make-instance 'godot-const :cs "System.Single"))
  (defparameter system-object (make-instance 'godot-const :cs "System.Object"))
  (defparameter transform (make-instance 'godot-const :cs "Godot.Transform, GodotSharp")))

;; functions
(progn
  (defparameter move-and-slide-fun (make-instance 'godot-fun :parent *box* :cs "MoveAndSlide"))
  (defparameter gd-print (make-instance 'godot-fun :parent "S_Godot.GD, GodotSharp" :cs "Print"))
  (defparameter write-line (make-instance 'godot-fun :parent "S_System.Console" :cs "WriteLine" :arg-types (list system-object)))
  (defparameter v2-add (make-instance 'godot-fun :parent gd-vec2-s :cs "op_Addition" :arg-types (list gd-vec2 gd-vec2)))
  (defparameter scalar-mul-v2 (make-instance 'godot-fun :parent gd-vec2-s :cs "op_Multiply" :arg-types (list system-single gd-vec2)))
  (defparameter scalar-mul-v3 (make-instance 'godot-fun :parent gd-vec3-s :cs "op_Multiply" :arg-types (list system-single gd-vec3)))
  (defparameter norm-v2 (make-instance 'godot-fun :parent gd-vec2-s :cs "Normalized"))
  (defparameter norm-v3 (make-instance 'godot-fun :parent gd-vec3-s :cs "Normalized"))
  (defparameter linear-interpolate (make-instance 'godot-fun
						  :parent *empty-vec3*
						  :cs "LinearInterpolate"
						  :arg-types (list system-single gd-vec3)))
  (defparameter deg-to-rad (make-instance 'godot-fun :parent "S_Godot.Mathf, GodotSharp" :cs "Deg2Rad"))
  (defparameter rotate-x (make-instance 'godot-fun :parent *player-rot* :cs "RotateX"))
  (defparameter rotate-y (make-instance 'godot-fun :parent *box* :cs "RotateY"))
  (defparameter clamp (make-instance 'godot-fun :parent "S_System.Math" :cs "Clamp" :arg-types (list system-single system-single system-single)))
  (defparameter is-on-floor (make-instance 'godot-fun :parent *box* :cs "IsOnFloor")))

(defmacro move-and-slide (obj x y z)
  `(progn
     (setf (arg-list move-and-slide-fun) `(t 0.785398 4 nil ,(make-instance 'vec3 :x 0.0 :y 1.0 :z 0.0) ,(vec3 :x ,x :y ,y :z ,z)))
     (push-args move-and-slide-fun)
     (push-to-stack ,obj)
     (push-to-stack move-and-slide-fun)))

(defmacro copy-vector (vec-b vec-a)
  `(progn
     (set-field "x" ,vec-a (get-field "x" ,vec-b))
     (set-field "y" ,vec-a (get-field "y" ,vec-b))
     (set-field "z" ,vec-a (get-field "z" ,vec-b))))

(defun reset-pos ()
  (if-op (:eq (get-field "devMode" *lisp-vm*) (get-field "devMode" *lisp-vm*))
      (progn
	(set-field "Transform" *box* (get-starting-location)))))

(defun add-basis-vectors ()
  (get-dir-vec3)
  (get-field "y" (get-normalized-vec2))
  (get-field "z" (get-field basis (get-field global-transform *camera*)))
  (with-return
    (push-to-stack scalar-mul-v3))
  (sub-vec3)
  (set-dir-vec3)
  (get-field "x" (get-normalized-vec2))
  (get-field "x" (get-field basis (get-field global-transform *camera*)))
  (with-return
    (push-to-stack scalar-mul-v3))
  (get-dir-vec3)
  (add-vec3)
  (set-dir-vec3))

(defun calculate-gravity ()
  (progn
    (defgdparameter max-speed 4)
    (defgdparameter max-slope-angle 7)
    (defgdparameter h-vel 12)
    (defgdparameter accel 26)
    (defgdparameter is-floored 25))
  ;; is the player on the ground
  (progn
    (with-return
      (push-to-stack *box*)
      (push-to-stack is-on-floor))
    (set-is-floored))
  (set-max-speed 20.0)
  (set-max-slope-angle 40.0)
  (set-field "y" (get-dir-vec3) 0.0)
  (with-return
    (gd-funcall norm-v3))
  (set-dir-vec3)
  (set-field "y" (get-vel-vec3) (gd+ (delta -24.8) (get-field "y" (get-vel-vec3))))
  (set-vel-vec3)
  (push-to-stack (make-instance 'vec3 :x 0.0 :y 0.0 :z 0.0))
  (set-h-vel)
  (copy-vector (get-vel-vec3) (get-h-vel))
  (set-field "y" (get-h-vel) 0.0)
  (defgdparameter target 9)
  (push-to-stack (make-instance 'vec3 :x 0.0 :y 0.0 :z 0.0))
  (set-target)
  (copy-vector (get-dir-vec3) (get-target))
  (get-max-speed)
  (get-target)
  (with-return
    (push-to-stack scalar-mul-v3))
  (set-target)
  (if-op (:great (dot-vec3 (get-dir-vec3) (get-h-vel)) 0.0)
      (delta 4.5)
      (delta 6.0))
  (set-accel)
  (if-op (:ne (get-is-floored) t)
      (progn
	(delta 2.0)
	(set-accel)))
  (get-accel)
  (get-target)
  (get-h-vel)
  (with-return
    (push-to-stack linear-interpolate))
  (set-h-vel)
  (set-field "x" (get-vel-vec3) (get-field "x" (get-h-vel)))
  (set-vel-vec3)
  (set-field "z" (get-vel-vec3) (get-field "z" (get-h-vel)))
  (set-vel-vec3))

(defun fps-turn ()
  (defgdparameter event 6)
  (defgdparameter mouse-sensitivity 8)
  (set-mouse-sensitivity 0.05)
  (defgdparameter mouse-y 20)
  (defgdparameter mouse-x 21)
  (defgdparameter camera-rot 22)
  (defgdparameter clamped-x 23)
  (set-event :push-input-event)
  (if-op (:eq "Captured" (get-mouse-mode))
      (if-op (:eq :push-ie-mousemotion (get-type (get-event)))
	  (progn
	    (gd* -1.0 (gd* (get-field "y" (get-field "Relative" (get-event))) (get-mouse-sensitivity)))
	    (set-mouse-y)
	    (gd-funcall rotate-x *player-rot* (with-return (gd-funcall deg-to-rad (get-mouse-y))))
	    (gd- 0.0 (get-field "x" (get-field "Relative" (get-event))))
	    (set-mouse-x)
	    (gd* (get-mouse-x) (get-mouse-sensitivity))
	    (set-mouse-x)
	    (gd-funcall rotate-y *box* (with-return (gd-funcall deg-to-rad (get-mouse-x))))
	    (set-camera-rot (make-instance 'vec3 :x 0.0 :y 0.0 :z 0.0))
	    (copy-vector (get-field "RotationDegrees" *player-rot*) (get-camera-rot))
	    (get-field "x" (get-camera-rot))
	    (set-clamped-x)
	    (with-return 
	      (push-to-stack 70.0)
	      (push-to-stack -70.0)
	      (get-clamped-x)
	      (push-to-stack clamp))
	    (set-clamped-x)
	    (set-field "x" (get-camera-rot) (get-clamped-x))
	    (set-camera-rot)
	    (set-field "RotationDegrees"
		       *player-rot*
		       (get-camera-rot))))))

(defun jump ()
  (if-op (:eq (get-is-floored) t)
      (progn
	(defgdparameter jump-speed 24)
	(set-jump-speed 9.0)
	(with-input-press
            *ui-accept*
	  (set-field "y" (get-vel-vec3) (get-jump-speed))))))

;; Character Movement
;; (in-package :crash)
(with-prelude (*spatial* *camera* *box* *player-rot* *ui-up* *ui-right* *ui-down* *ui-left* *ui-cancel* *ui-reset* *ui-accept* *ui-select*
			 gd-vec2 gd-vec3 gd-vec2-s gd-vec3-s system-single system-object *lisp-vm*
			 basis global-transform linear-interpolate transform
			 move-and-slide-fun v2-add rotate-x rotate-y gd-print norm-v2 norm-v3 scalar-mul-v2 scalar-mul-v3 deg-to-rad clamp write-line
			 is-on-floor)
  (labels ((vec2 (&key x y)
	     (funcall (make-constructor 2 gd-vec2) x y)
	     (null-op))
	   (add-vec2 (&optional (vec-a nil a-supplied-p) (vec-b nil b-supplied-p))
	     (if a-supplied-p
		 (push-to-stack vec-a))
	     (if b-supplied-p
		 (push-to-stack vec-b))
	     (with-return
	       (push-to-stack v2-add))))
    (with-phys-process
      (progn ;; temp-declarations
	(defgdparameter dir-vec3 0)
	(defgdparameter vel-vec3 10)
	(defgdparameter normalized-vec2 1)
	(defgdparameter starting-location 3))
      (if-op (:is-null (starting-location-id))
	  (set-starting-location (get-field "Transform" *box*)))      
      (set-dir-vec3 (vec3 :x 0.0 :y 0.0 :z 0.0))
      (if-op (:is-null (vel-vec3-id))
	  (set-vel-vec3 (vec3 :x 0.0 :y 0.0 :z 0.0)))
      (vec2 :x 0.0 :y 0.0)
      (with-input-press
          *ui-up*
	(add-vec2 (vec2 :x 0.0 :y 1.0)))
      (with-input-press
          *ui-down*
	(add-vec2 (vec2 :x 0.0 :y -1.0)))
      (with-input-press
          *ui-left*
	(add-vec2 (vec2 :x -1.0 :y 0.0)))
      (with-input-press
          *ui-right*
	(add-vec2 (vec2 :x 1.0 :y 0.0)))
      (with-return
	(gd-funcall norm-v2))
      (set-normalized-vec2)
      (with-input-just
	  *ui-reset*
	(reset-pos))
      (add-basis-vectors)
      (calculate-gravity)
      (jump)
      (with-return
	(move-and-slide *box*
			(get-field "x" (get-vel-vec3))
			(get-field "y" (get-vel-vec3))
			(get-field "z" (get-vel-vec3))))
      (set-vel-vec3)))
  (with-event
    (with-input-just
	*ui-cancel*
      (if-op (:eq "Visible" (get-mouse-mode))
	  (set-mouse-mode "Captured")
	  (set-mouse-mode "Visible")))
    (with-input-just
	*ui-select*
      (if-op (:eq "Visible" (get-mouse-mode))
	  (set-mouse-mode "Captured"))))
  (with-event
    (fps-turn)))


#|
(clear-model)
(clear-vm)
(clear :all)
(compile-bytecode "crash")
(add-to-prelude linear-interpolate)
(send-uints)
(add-to-prelude linear-interpolate)
(add-to-prelude linear-interpolate)
(arg-types linear-interpolate)
(in-package :crash)
(operations:reverse-op 7)
(decode 400)
(decode-prelude 400)
(push-to-stack (length (arg-types linear-interpolate)) t)
(arg-types linear-interpolate)
(type-of (length (arg-types linear-interpolate)))
(terpri)
(mapcar #'(lambda (a) (push-to-stack a t) (push-to-stack :type t)) (arg-types linear-interpolate))
(uint-id v2-add)
push-float something 
push-float 0
push-int32 2
push-string 
(code-char 71)
|#

