(defpackage model
  (:use :cl :operations :repl)
  (:export generate-prelude
	   godot
	   godot-const
	   godot-fun
	   godot-object
	   clear-model
	   clear-vm
	   add-to-prelude
	   push-id-to-stack
	   push-to-stack
	   push-to-stack-or-delay
	   push-args
	   push-caller
	   push-cs
	   vec3
	   x
	   y
	   z
	   uint-id
	   cs
	   parent
	   arg-list
	   caller
	   null-op
	   get-field
	   set-field
	   gd-funcall
	   define-operator
	   gd+
	   gd-
	   gd*
	   gd/
	   uint-id
	   arg-types))

(in-package model)

(defvar *obj-count* -1)
(defvar *fun-count* -1)
(defvar *const-count* -1)
(defvar *objects* '())
(defvar *functions* '())
(defvar *constants* '())

(defun clear-model ()
  (setf *obj-count* -1
	*fun-count* -1
	*const-count* -1
	*objects* '()
	*functions* '()
	*constants* '()))

(defun clear-vm ()
  (clear :all)
  (insert-uint (op :clear))
  (send-uints)
  (sleep 0.8)
  (clear :all))

(defclass godot ()
  ((uint-id
    :accessor uint-id)
   (cs
    :initarg :cs
    :accessor cs)))

(defclass null-op ()
  ())

(defclass godot-object (godot)
  ())

(defclass godot-fun (godot)
  ((parent
    :initarg :parent
    :accessor parent)
   (caller
    :initarg :caller
    :initform nil
    :accessor caller)
   (arg-list
    :initarg :arg-list
    :initform nil
    :accessor arg-list)
   (arg-types
    :initarg :arg-types
    :initform nil
    :accessor arg-types)))

(defclass godot-const (godot)
  ())

(defclass vec3 ()
  ((x
     :initarg :x
     :initform 0.0
     :accessor x)
   (y
     :initarg :y
     :initform 0.0
     :accessor y)
   (z
     :initarg :z
     :initform 0.0
     :accessor z)))

(defmethod initialize-instance :after ((obj godot-object) &key)
  (setf (uint-id obj) (incf *obj-count*)))

(defmethod initialize-instance :after ((fun godot-fun) &key)
  (setf (uint-id fun) (incf *fun-count*)))

(defmethod initialize-instance :after ((const godot-const) &key)
  (setf (uint-id const) (incf *const-count*)))

(defgeneric push-id-to-stack (gd &optional prelude aux)
  (:documentation
   "Push this gd instance's id onto the VM's stack at uint pointer. Pass t to send to prelude."))

(defmethod push-id-to-stack ((gd godot) &optional prelude aux)
  (insert-uint (op :push-immediate) prelude aux)
  (insert-uint (uint-id gd) prelude aux))

(defun push-bool (b &optional prelude aux)
  (if b
      (insert-uint (op :T) prelude aux)
      (insert-uint (op :NIL) prelude aux)))

(defgeneric push-to-stack (gd &optional prelude aux)
  (:documentation
   "Push this gd instance onto the stack using its appropriate opcode + its instance id"))

(defmacro push-to-stack-or-delay (evaluated-obj &optional prelude aux)
  `(progn
     (if (equal (type-of ',evaluated-obj) 'CONS)
	 (push-to-stack (lambda () ,evaluated-obj) ,prelude ,aux)
	 (push-to-stack ,evaluated-obj ,prelude ,aux))))

(defmethod push-to-stack ((obj godot-object) &optional prelude aux)
  (push-id-to-stack obj prelude aux)
  (insert-uint (op :obj) prelude aux))

(defmethod push-to-stack ((fun godot-fun) &optional prelude aux)
  (push-id-to-stack fun prelude aux)
  (insert-uint (op :fun) prelude aux)
  (insert-uint (op :invoke) prelude aux))

(defmethod push-to-stack ((evaluated-obj CONS) &optional prelude aux)
  `(progn
     (push-to-stack (lambda () ,evaluated-obj) ,prelude ,aux)
     (push-to-stack ,evaluated-obj ,prelude ,aux)))

(defmacro gd-funcall (fun &rest args)
  `(progn
     (loop :for arg :in (list ,@(reverse args))
	   :do (progn
		 (if (equal (type-of arg) 'CONS)
		     (push-to-stack (lambda () arg))
		     (push-to-stack arg))))
     (push-to-stack ,fun)
     (null-op)))

(defmethod push-args ((fun godot-fun) &optional prelude aux)
  (when (arg-list fun)
    (mapcar (lambda (arg) (push-to-stack arg prelude aux)) (arg-list fun))))

(defmethod push-caller ((fun godot-fun) &optional prelude aux)
    (if (caller fun)
      (push-to-stack (caller fun) prelude aux)
      (push-to-stack (parent fun) prelude aux)))

(defmethod push-to-stack ((const godot-const) &optional prelude aux)
  (push-id-to-stack const prelude aux)
  (insert-uint (op :const-str) prelude aux))

(defmethod push-to-stack ((num single-float) &optional prelude delta)
  (if delta
      (insert-uint (op :push-delta-float) prelude)
      (insert-uint (op :push-float) prelude))

  (ffi:with-foreign-object (float-arr '(:array :float 1))
    (setf (ffi:deref-array float-arr '(:array :float) 0) num)
    (insert-uint (ffi:deref-array float-arr '(:array :uint32-t) 0) prelude)))

(defmethod push-to-stack ((num fixnum) &optional prelude aux)
  (insert-uint (op :push-int32) prelude)
  (ffi:with-foreign-object (int-arr '(:array :int32-t 1))
    (setf (ffi:deref-array int-arr '(:array :int32-t) 0) num)
    (insert-uint (ffi:deref-array int-arr '(:array :uint32-t) 0) prelude aux)))

(defmethod push-to-stack ((bool (eql 'T)) &optional prelude aux)
  (push-bool t prelude aux))

(defmethod push-to-stack ((bool NULL) &optional prelude aux)
  (push-bool nil prelude aux))

(defmethod push-to-stack ((vec vec3) &optional prelude aux)
  (push-to-stack (x vec) prelude aux)
  (push-to-stack (y vec) prelude aux)
  (push-to-stack (z vec) prelude aux)
  (insert-uint (op :vec) prelude aux))

(defmethod push-to-stack ((key KEYWORD) &optional prelude aux)
  (let ((keys-with-double-meta '(:end-string
				 :end-proc
				 :end-input
				 :end-event
				 :else
				 :endif
				 :else-input)))
    (insert-uint (op key) prelude aux)
    (if (member key keys-with-double-meta)
	(progn
	  (insert-uint (op :meta) prelude aux)
	  (insert-uint (op :meta) prelude aux)))))

(defmethod push-to-stack ((function FUNCTION) &optional prelude aux)
  (funcall function))

(defmethod push-to-stack ((char-arr string) &optional prelude aux)
  (insert-uint (op :push-string) prelude aux)
  (ffi:with-foreign-object (st `(:array :char ,(length char-arr)))
  (loop 
	:for i :from 0 :to (- (length char-arr) 1)
	:do (progn
	      (setf (ffi:deref-array st '(:array :char) i) (aref char-arr i))
	      (insert-uint (ffi:deref-array st '(:array :byte) i) prelude aux))))
  (push-to-stack :end-string prelude aux))

(defmethod push-to-stack ((n-op null-op) &optional prelude aux))

(defgeneric push-cs (gd &optional prelude aux)
  (:documentation
   "Push C# string representation of a gd instance"))

(defmethod push-cs ((gd godot) &optional prelude aux)
  (insert-uint (op :push-string) prelude aux)
  (ffi:with-foreign-object (st `(:array :char ,(length (cs gd))))
  (loop 
	:for i :from 0 :to (- (length (cs gd)) 1)
	:do (progn
	      (setf (ffi:deref-array st '(:array :char) i) (aref (cs gd) i))
	      (insert-uint (ffi:deref-array st '(:array :byte) i) prelude aux))))
  (push-to-stack :end-string prelude aux))

(defgeneric add-to-prelude (gd)
  (:documentation
   "Register instance's uint-id in prelude so that main will interpret and push correct gd"))

(defmethod add-to-prelude ((obj godot-object))
  (push-cs obj t)
  (push-id-to-stack obj t)
  (insert-uint (op :reg-obj) t))

(defmethod add-to-prelude ((fun godot-fun))
  (if (arg-types fun)
      (progn
	(let ((num-args (length (arg-types fun))))
	  (mapcar #'(lambda (arg-type) (push-to-stack arg-type t) (push-to-stack :type t)) (arg-types fun))
	  (push-to-stack num-args t)))
      (progn
	(push-to-stack 0 t)))
  (push-to-stack (parent fun) t)
  (push-cs fun t)
  (push-id-to-stack fun t)
  (insert-uint (op :reg-fun) t))

(defmethod add-to-prelude ((const godot-const))
  (push-cs const t)
  (push-id-to-stack const t)
  (insert-uint (op :reg-const-str) t))

;; Credits to Vatine and Joshua Taylor for the operator-shadowing explanations
;; https://stackoverflow.com/questions/25152029/override-overload-the-operator-to-operate-on-common-lisp-vectors
(defmacro define-operator (op &key (binary-signifier :godot) (package *package*))
  "Defines a generic operator OP, being essentially a reduce operation using
   a generic function whose name is a concatenation of BINARY-SIGNIFIER and OP."
  (let ((op op)
        (binary (intern (concatenate 'string
                                     (string  binary-signifier)
                                     (string op))
                        package)))
    `(progn
       (defun ,op (&rest args)
         (reduce (function ,binary) (cdr args) :initial-value (car args)))
       (defgeneric ,binary (arg1 arg2)))))

(defmethod godot+ ((x number) (y number))
  (cl:+ x y))

(defmethod godot+ ((obj godot-object) (obj2 godot-object))
  (push-to-stack obj2)
  (push-to-stack obj)
  (push-to-stack :add))

(defmethod godot- ((x number) (y number))
  (cl:- x y))

(defmethod godot- ((obj godot-object) (obj2 godot-object))
  (push-to-stack obj2)
  (push-to-stack obj)
  (push-to-stack :sub))

(defmethod godot* ((x number) (y number))
  (cl:* x y))

(defmethod godot* ((obj godot-object) (obj2 godot-object))
  (push-to-stack obj2)
  (push-to-stack obj)
  (push-to-stack :mul))

(defmethod godot/ ((x number) (y number))
  (cl:/ x y))

(defmethod godot/ ((obj godot-object) (obj2 godot-object))
  (push-to-stack obj2)
  (push-to-stack obj)
  (push-to-stack :div))

(defmacro gd+ (float1 float2)
  `(progn
     (push-to-stack-or-delay ,float2)
     (push-to-stack-or-delay ,float1)
     (push-to-stack :add)))

(defmacro gd- (float1 float2)
  `(progn
     (push-to-stack-or-delay ,float1)
     (push-to-stack-or-delay ,float2)
     (push-to-stack :sub)))

(defmacro gd* (float1 float2)
  `(progn
     (push-to-stack-or-delay ,float2)
     (push-to-stack-or-delay ,float1)
     (push-to-stack :mul)))

(defmacro gd/ (float1 float2)
  `(progn
     (push-to-stack-or-delay ,float1)
     (push-to-stack-or-delay ,float2)
     (push-to-stack :div)))

(defun null-op ()
  (make-instance 'null-op))

(defmacro set-field (field obj val)
  `(progn
    (push-to-stack-or-delay ,obj)
    (push-to-stack-or-delay ,field)
    (push-to-stack-or-delay ,val)
    (insert-uint (op :set))
    (null-op)))

(defmacro get-field (field obj)
  `(progn
     (push-to-stack-or-delay ,obj)
     (push-to-stack-or-delay ,field)
     (insert-uint (op :get))
     (null-op)))

