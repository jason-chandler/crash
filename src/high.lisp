(defpackage high
  (:use :cl :model :operations :repl)
  (:export with-prelude
	   with-process
	   with-phys-process
	   with-input-press
	   with-input-just
	   with-input-press-proc
	   with-input-just-proc
	   with-event
	   set-mouse-mode
	   get-mouse-mode
	   if-op
	   get-type
	   vec3
	   push-vec3
	   add-vec3
	   sub-vec3
	   cross-vec3
	   dot-vec3
	   get-local
	   set-local
	   to-array
	   make-constructor
	   with-return
	   defgdparameter
	   delta
	   true-null))

(in-package :high)

;;(defvar *spatial* (make-instance 'godot-object :cs "SPATIAL"))
;;(defvar *move-and-slide* (make-instance 'godot-fun :parent *spatial* :cs "MoveAndSlide"))

(defmacro with-prelude (class-list &body body)
  `(progn
     (mapcar #'add-to-prelude (list ,@class-list))
     ,@body))

(defmacro with-process (&body body)
  `(progn
     (push-to-stack :proc)
     ,@body
     (push-to-stack :ret)
     (push-to-stack :end-proc)))

(defmacro with-phys-process (&body body)
  `(progn
     (push-to-stack :phys-proc)
     ,@body
     (push-to-stack :ret)
     (push-to-stack :end-proc)))

(defmacro with-input-press (action &body body)
  `(progn
     (push-to-stack ,action)
     (push-to-stack :input-press)
     ,@body
     (push-to-stack :end-input)))

(defmacro with-input-just (action &body body)
  `(progn
     (push-to-stack ,action)
     (push-to-stack :input-just)
     ,@body
     (push-to-stack :end-input)))

(defmacro with-input-press-proc (action &body body)
  `(progn
     (push-to-stack :phys-proc)
     (push-to-stack ,action)
     (push-to-stack :input-press)
     ,@body
     (push-to-stack :end-input)
     (push-to-stack :end-proc)))

(defmacro with-input-just-proc (action &body body)
  `(progn
     (push-to-stack :phys-proc)
     (push-to-stack ,action)
     (push-to-stack :input-just)
     ,@body
     (push-to-stack :end-input)
     (push-to-stack :end-proc)))

(defmacro with-event (&body body)
  `(progn
     (push-to-stack :event)
     ,@body
     (push-to-stack :end-event)))

(defun set-mouse-mode (mode)
  (cond ((equal mode "Visible") (push-to-stack 0))
	((equal mode "Hidden") (push-to-stack 1))
	((equal mode "Captured") (push-to-stack 2))
	((equal mode "Confined") (push-to-stack 3)))
  (push-to-stack :input-set-mousemode))

(defun get-mouse-mode ()
  (push-to-stack :input-get-mousemode))

(defmacro if-op (condition then &optional (else-block nil else-supplied-p))
  `(progn
     (push-to-stack-or-delay ,(cadr condition))
     (unless (eql ,(car condition) :is-null)
	     (push-to-stack-or-delay ,(caddr condition)))
     (push-to-stack ,(car condition))
     (progn
       ,then)
     (if ,else-supplied-p
	  (progn
	    (push-to-stack :else)
	    ,else-block))
     (push-to-stack :endif)))

(defun get-type (obj)
  (push-to-stack obj)
  (push-to-stack :type))

(defmacro vec3 (&key x y z)
  `(progn
     (make-instance 'vec3
		    :x (lambda () (push-to-stack ,x))
		    :y (lambda () (push-to-stack ,y))
		    :z (lambda () (push-to-stack ,z)))))

(defun push-vec3 (&key x y z)
  (push-to-stack (vec3 :x x :y y :z z)))

(defun get-local (local-uint)
  (push-to-stack :push-immediate)
  (insert-uint local-uint)
  (push-to-stack :get-local)
  (null-op))

(defun set-local (local-uint &optional (obj nil obj-supplied-p))
  (when obj-supplied-p
      (push-to-stack-or-delay obj))
  (push-to-stack :push-immediate)
  (insert-uint local-uint)
  (push-to-stack :set-local)
  (null-op))

;; (in-package :high)
(defmacro defgdparameter (gd-symbol assigned-uint &optional (package *package*))
  (let* ((gd-symbol gd-symbol)
	 (get-gd-fun-sym (intern (concatenate 'string
					      "GET-"
					      (string gd-symbol))
				 package))
	 (set-gd-fun-sym (intern (concatenate 'string
					      "SET-"
					      (string gd-symbol))
				 package))
	 (gd-fun-sym-id (intern (concatenate 'string
					     (string gd-symbol)
					     "-ID")
				 package))
	(assigned-uint assigned-uint))
    `(progn
       (defun ,get-gd-fun-sym ()
	 (get-local ,assigned-uint))
       (defun ,set-gd-fun-sym (&optional (new-val nil new-val-supplied-p))
	 (if new-val-supplied-p
	     (set-local ,assigned-uint new-val)
	     (set-local ,assigned-uint)))
       (defun ,gd-fun-sym-id ()
	 (push-to-stack ,assigned-uint)))))

(defun add-vec3 (&optional (vec-a nil a-supplied-p) (vec-b nil b-supplied-p))
  (if a-supplied-p
      (push-to-stack vec-a))
  (if b-supplied-p
      (push-to-stack vec-b))
  (push-to-stack :vadd))

(defun sub-vec3 (&optional (vec-a nil a-supplied-p) (vec-b nil b-supplied-p))
  (if a-supplied-p
      (push-to-stack vec-a))
  (if b-supplied-p
      (push-to-stack vec-b))
  (push-to-stack :vsub))

(defun dot-vec3 (&optional (vec-a nil a-supplied-p) (vec-b nil b-supplied-p))
  (if a-supplied-p
      (push-to-stack vec-a))
  (if b-supplied-p
      (push-to-stack vec-b))
  (push-to-stack :vdot))

(defun cross-vec3 (&optional (vec-a nil a-supplied-p) (vec-b nil b-supplied-p))
  (if a-supplied-p
      (push-to-stack vec-a))
  (if b-supplied-p
      (push-to-stack vec-b))
  (push-to-stack :vcross))

(defun to-array (&rest array-items)
  (dolist (array-item array-items)
    (push-to-stack array-item))
  (push-to-stack (length array-items))
  (push-to-stack :array))

(defun make-constructor (nargs obj)
  (if (= nargs 0)
      (lambda ()
	(push-to-stack nargs)
	(push-to-stack obj)
	(push-to-stack :new))
      (lambda (&rest args)
	(dolist (arg (reverse args))
	  (push-to-stack arg))
	(push-to-stack nargs)
	(push-to-stack obj)
	(push-to-stack :new))))

(defmacro with-return (&body body)
  `(progn
     ,@body
     (push-to-stack :push-return)
     (null-op)))

(defun delta (&optional (scalar nil scalar-supplied-p))
  (if scalar-supplied-p
      (push-to-stack-or-delay scalar nil t)
      (push-to-stack-or-delay 1.0 nil t)))

(defun true-null ()
  (push-to-stack :push-null))

#|
(defmacro get-prop (obj prop)
  `(progn
     (push-to-stack ,prop)
     (push-to-stack ,obj)
     (push-to-stack :get)))

(defmacro set-prop (obj prop val)
  `(progn
     (push-to-stack ,val)
     (push-to-stack ,prop)
     (push-to-stack ,obj)
     (push-to-stack :set)))



;; MouseMovement Example


(with-prelude (*spatial* *box* *move-and-slide*)
     (set-mouse-mode "captured")
     (with-event
       (if-op :eq :push-ie-mousemotion (get-type :push-input-event)
	   ((move-and-slide *box* 25.0 0.0 0.0))
           ((move-and-slide *box* -75.0 0.0 0.0)))))

(with-prelude (*spatial* *box* *move-and-slide*)
     (set-mouse-mode "captured")
     (with-event
       (if-op :eq :push-ie-mousemotion (get-type *box*)
	   ((move-and-slide *box* 25.0 0.0 0.0))
           ((if-op :eq :push-ie-mousemotion (get-type :push-input-event)
                ((move-and-slide *box* 25.0 0.0 0.0)))))))

 (clear :all)
 (clear-model)
 (clear-vm)
 
(with-prelude (crash::*box*)
 (get-prop crash::*box* "Translation"))
(type-of "test")
 (repl:print-uints 0 68)
 (repl:decode 160)
 (repl:print-uints 0 12 t)
 (repl:clear :all)

 (send-uints)

 (set-up-repl)

(progn
 (defparameter *spatial* (make-instance 'godot-object :cs "SPATIAL"))
 (defparameter *box* (make-instance 'godot-object :cs "BOX"))
 (defparameter *sphere* (make-instance 'godot-object :cs "SPHERE"))
 (defparameter *move-and-slide* (make-instance 'godot-fun :parent *spatial* :cs "MoveAndSlide"))
 (defparameter *rotate-y* (make-instance 'godot-fun :parent *box* :cs "RotateY"))
 (defparameter *ui-up* (make-instance 'godot-const :cs "ui_up"))
 (defparameter *ui-right* (make-instance 'godot-const :cs "ui_right"))
 (defparameter *ui-left* (make-instance 'godot-const :cs "ui_left"))
 (defparameter *ui-down* (make-instance 'godot-const :cs "ui_down")))

(with-prelude (*spatial* *box* *rotate-y* *move-and-slide* *ui-up* *ui-right* *ui-down* *ui-left* *translation*)
    (with-input-press
        *ui-up*
        (move-and-slide *box* 0.0 25.0 0.0))

    (with-input-press
        *ui-down*
        (move-and-slide *box* 0.0 -25.0 0.0)))


 (defparameter *translation* (make-instance 'godot-const :cs "Translation")))
 (clear :all)
 (clear-model)
 (clear-vm)
 
 (repl:print-uints 0 70)
 (repl:print-uints 0 12 t)
 (repl:clear :all)

 (send-uints)

(uint-id *box*)

(push-to-stack *box*)

(setf (arg-list *move-and-slide*) `(t 0.785398 4 nil ,(make-instance 'vec3 :x 0.0 :y 1.0 :z 0.0) ,(make-instance 'vec3 :x 0.0 :y 10.0 :z 0.0)))
(push-to-stack *move-and-slide*)

(with-prelude (*ui-up*)
    (push-to-stack *ui-up*))

(with-process 
    (push-to-stack *rotate-y* nil t))

(with-prelude (*spatial* *box* *rotate-y* *move-and-slide* *ui-up* *ui-right*)
(with-input-press
    *ui-up*
    (setf (arg-list *rotate-y*) '(5.5))
    (push-to-stack *rotate-y* nil t))

(with-input-press
    *ui-right*
    (setf (arg-list *rotate-y*) '(-5.5))
    (push-to-stack *rotate-y* nil t)))

(with-prelude (*world* *box* *rotate-y* *move-and-slide* *ui-up* *ui-right* *translation*)
    (set-field *translation* *box* (get-field *translation* *sphere*)))

(with-prelude (*spatial* *box* *rotate-y* *move-and-slide* *ui-up* *ui-right* *ui-down* *ui-left* *translation*)
    (with-input-press
        *ui-up*
        (move-and-slide *box* 0.0 5.0 0.0))

    (with-input-press
        *ui-down*
        (move-and-slide *box* 0.0 -5.0 0.0)))

;; MouseMovement Example

(with-prelude (*spatial* *box* *move-and-slide*)
  (push-to-stack 2)
  (push-to-stack :input-set-mousemode))
  (push-to-stack :event))
  (push-to-stack :push-ie-mousemotion))
  (push-to-stack :push-input-event))
  (push-to-stack :type))
  (push-to-stack :eq))
  (move-and-slide *box* 5.0 0.0 0.0)
  (push-to-stack :endif))
  (push-to-stack :end-event)))


|#

