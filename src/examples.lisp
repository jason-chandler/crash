(set-up-repl)

(progn
 (defparameter *spatial* (make-instance 'godot-object :cs "SPATIAL"))
 (defparameter *box* (make-instance 'godot-object :cs "BOX"))
 (defparameter *sphere* (make-instance 'godot-object :cs "SPHERE"))
 (defparameter *camera* (make-instance 'godot-object :cs "BOX/CAMERA"))
 (defparameter *move-and-slide* (make-instance 'godot-fun :parent *box* :cs "MoveAndSlide"))
 (defparameter *gd-print* (make-instance 'godot-fun :parent "S_Godot.GD, GodotSharp" :cs "Print"))
 (defparameter *v2-add* (make-instance 'godot-fun :parent "S_Godot.Vector2, GodotSharp" :cs "op_Addition"))
 (defparameter *rotate-y* (make-instance 'godot-fun :parent *box* :cs "RotateY"))
 (defparameter *ui-up* (make-instance 'godot-const :cs "ui_up"))
 (defparameter *ui-right* (make-instance 'godot-const :cs "ui_right"))
 (defparameter *ui-left* (make-instance 'godot-const :cs "ui_left"))
 (defparameter *ui-down* (make-instance 'godot-const :cs "ui_down"))
 (defparameter *ui-cancel* (make-instance 'godot-const :cs "ui_cancel")))

(defmacro move-and-slide (obj x y z)
  `(progn
     (setf (arg-list *move-and-slide*) `(t 0.785398 4 nil ,(make-instance 'vec3 :x 0.0 :y 1.0 :z 0.0) ,(vec3 :x ,x :y ,y :z ,z)))
     (push-args *move-and-slide*)
     (push-to-stack ,obj)
     (push-to-stack *move-and-slide*)))


;; Character Movement
(with-prelude (*spatial* *camera* *box* *ui-up* *ui-right* *ui-down* *ui-left* *ui-cancel* *move-and-slide* *v2-add* *rotate-y* *gd-print*)
  (labels ((push-vec2 (&key x y) (funcall (make-constructor 2 "Godot.Vector2, GodotSharp") x y)))
    (with-phys-process
      (push-vec3 :x 0.0 :y 0.0 :z 0.0)
      (with-input-press
          *ui-up*
	(add-vec3 (vec3 :x 0.0 :y 1.0 :z 0.0)))
      (with-input-press
          *ui-down*
	(add-vec3 (vec3 :x 0.0 :y -1.0 :z 0.0)))
      (with-input-press
          *ui-left*
	(add-vec3 (vec3 :x -1.0 :y 0.0 :z 0.0)))
      (with-input-press
          *ui-right*
	(add-vec3 (vec3 :x 1.0 :y 0.0 :z 0.0)))
      (set-local 0)
      (move-and-slide *box*
		      (get-field "x" (get-local 0))
		      (get-field "y" (get-local 0))
		      (get-field "z" (get-local 0)))))

  (with-input-just-proc
      *ui-cancel*
    (if-op (:eq "Visible" (get-mouse-mode))
	(set-mouse-mode "Captured")
	(set-mouse-mode "Visible"))
  ))


;; using to-array and static classes
(with-prelude (*spatial* *camera* *box* *move-and-slide* *rotate-y* *gd-print*
			 *ui-up* *ui-right* *ui-down* *ui-left* *ui-cancel*)
  (to-array "hi" "hello")
  (push-to-stack *gd-print*))




;; static method from non-predefined type

(with-prelude (*spatial* *camera* *box* *ui-up* *ui-right* *ui-down* *ui-left* *ui-cancel* *move-and-slide* *v2-add* *rotate-y* *gd-print*)
  (labels ((push-vec2 (&key x y) (funcall (make-constructor 2 "Godot.Vector2, GodotSharp") x y)))
    (push-vec2 :x 1.0 :y 2.0)
    (push-vec2 :x 1.0 :y 4.0)
    (push-to-stack *v2-add*)))




#| I can't remember this stuff
(clear-model)
(clear-vm)
(clear :all)
(send-uints)

(with-prelude (*spatial* *camera* *box* *move-and-slide* *rotate-y* *gd-print*
			 *ui-up* *ui-right* *ui-down* *ui-left* *ui-cancel*)
  

    (flet ((push-vec2 (x y) (funcall (make-constructor 2 "Godot.Vector2, GodotSharp") x y)))
       (push-vec2 1.0 2.0)
       (set-local 0)
       (get-field "x" (get-local 0))
       (get-field "y" (get-local 0))))
(in-package :crash)

|#

#|
(get-field "Translation" *box*)

(get-field "name" (get-local 1))

(with-prelude (*spatial* *camera* *box* *rotate-y* *move-and-slide* *ui-up* *ui-right* *ui-down* *ui-left*)
    (with-input-press
        *ui-up*
        (move-and-slide *box* 0.0 5.0 0.0)))

(with-prelude (*spatial* *box* *rotate-y* *move-and-slide* *ui-up* *ui-right* *ui-down* *ui-left*)
    (set-field "y" (get-field "Translation" *box*) 110.0))

(with-prelude (*spatial* *box* *sphere* *move-and-slide* *rotate-y* *ui-up* *ui-right* *ui-down* *ui-left*)
    (set-field "Translation" *box* (make-instance 'vec3 :x (lambda () (get-field "x" (get-field "Translation" *sphere*))) :y (lambda () (get-field "y" (get-field "Translation" *sphere*))) :z (lambda () (get-field "z" (get-field "Translation" *sphere*))))))

(with-prelude (*spatial* *box* *move-and-slide* *rotate-y* *ui-up* *ui-right* *ui-down* *ui-left*)
    (set-field "Translation" *box* (make-instance 'vec3 :x 0.0 :y 110.0 :z 0.0)))


(with-prelude (*spatial* *box* *rotate-y* *move-and-slide* *ui-up* *ui-right* *ui-down* *ui-left*)
    (with-input-press
        *ui-up*
        (get-field "Translation" *box*)))


(with-prelude (*spatial* *box* *move-and-slide*)
     (set-mouse-mode "captured")
     (with-event
       (if-op :eq :push-ie-mousemotion (get-type :push-input-event)
	   ((move-and-slide *box* 25.0 0.0 0.0))
           ((move-and-slide *box* -75.0 0.0 0.0)))))


(with-prelude (*spatial* *box* *move-and-slide*)
     (set-mouse-mode "captured")
     (with-event
       (if-op :eq :push-ie-mousemotion (get-type *box*)
	   ((move-and-slide *box* 25.0 0.0 0.0))
           ((if-op :eq :push-ie-mousemotion (get-type :push-input-event)
                ((move-and-slide *box* 25.0 0.0 0.0)))))))

(with-prelude (*spatial* *box* *sphere* *move-and-slide* *rotate-y* *ui-up* *ui-right* *ui-down* *ui-left*)
  (set-field "Translation" *box*
	     (vec3
	      :x (get-field "x" (get-field "Translation" *sphere*))
	      :y (get-field "y" (get-field "Translation" *sphere*))
	      :z (get-field "z" (get-field "Translation" *sphere*)))))

(progn
  (push-to-stack (vec3 :x 0.0 :y 0.0 :z 0.0))
  (push-to-stack (vec3 :x 1.0 :y 0.0 :z 0.0))
  (push-to-stack :vadd)
  (set-local 0)
  (get-local 0))

|#
