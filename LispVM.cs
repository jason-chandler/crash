using Godot;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Crash
{
	class LispVM
	{
		public static bool devMode = false;
		public static bool debug = false;
		public int pos = 0;
		public float dt;
		public InputEvent @event = null;
		public const long ARRAY_SIZE = 20480;
		private Stack stack = new Stack();
		private Stack truthStack = new Stack();
		private Stack inputTruthStack = new Stack();
		public byte[] bytecode = new byte[ARRAY_SIZE];
		public MemoryMappedFile preludeHandle;
		public MemoryMappedViewAccessor preludePbuf;
		public MemoryMappedFile handle;
		public MemoryMappedViewAccessor pbuf;
		public Hashtable registeredFunctions = new Hashtable();
		public Hashtable registeredObjects = new Hashtable();
		public ArrayList registeredProcesses = new ArrayList();
		public ArrayList registeredPhysProcesses = new ArrayList();
		public ArrayList registeredEvents = new ArrayList();
		public Hashtable registeredConstants = new Hashtable();
		public object[] registeredLocals = new object[256];
		private object returnRegister = null;
		public bool fileLoaded = false;

		public enum OpCode : uint
		{
			OP_RET,
			OP_T,
			OP_NIL,
			OP_CLEAR,
			OP_PUSH_IMMEDIATE,
			OP_CONST_STR,
			OP_PUSH_FLOAT,
			OP_PUSH_DELTA_FLOAT,
			OP_PUSH_INT32,
			OP_OBJ,
			OP_FUN,
			OP_PUSH_STRING,
			OP_END_STRING,
			OP_POP,
			OP_REG_OBJ,
			OP_REG_FUN,
			OP_REG_CONST_STR,
			OP_ADD,
			OP_SUB,
			OP_MUL,
			OP_DIV,
			OP_MOD,
			OP_VEC,
			OP_VADD,
			OP_VSUB,
			OP_VCROSS,
			OP_VDOT,
			OP_VMUL_SC,
			OP_VDIV_SC,
			OP_GET,
			OP_SET,
			OP_EQ,
			OP_NE,
			OP_LESS,
			OP_LE,
			OP_GE,
			OP_GREAT,
			OP_ELSE,
			OP_ENDIF,
			OP_PROC,
			OP_PHYS_PROC,
			OP_END_PROC,
			OP_INPUT_PRESS,
			OP_INPUT_JUST,
			OP_END_INPUT,
			OP_INVOKE,
			OP_INPUT_GET_MOUSEMODE,
			OP_INPUT_SET_MOUSEMODE,
			OP_PUSH_INPUT_EVENT,
			OP_PUSH_IE_MOUSEMOTION,
			OP_EVENT,
			OP_END_EVENT,
			OP_TYPE,
			OP_SET_LOCAL,
			OP_GET_LOCAL,
			OP_ELSE_INPUT,
			OP_ARRAY,
			OP_NEW,
			OP_META,
			OP_PUSH_RETURN,
			OP_PUSH_NULL,
			OP_IS_NULL
		}

		public LispVM()
		{
			if (LispVM.devMode)
			{
				this.preludeHandle = MemoryMappedFile.OpenExisting("PreludeLispMappingObject");
				this.handle = MemoryMappedFile.OpenExisting("MainLispMappingObject");
				this.preludePbuf = this.preludeHandle.CreateViewAccessor(0, ARRAY_SIZE);
				this.pbuf = this.handle.CreateViewAccessor(0, ARRAY_SIZE);
			} else
			{
				this.LoadLispByteCodeFromFile();
			}
		}

		public LispVM(ArrayList arr)
		{
			this.bytecode = new byte[arr.Count];
			arr.CopyTo(this.bytecode, 0);
		}

		byte ReadByte()
		{
			byte curByte = bytecode[pos];
			pos += 1;
			return curByte;
		}

		uint ReadUint()
		{
			if (pos < bytecode.Length)
			{
				//PrintIfDebug("ReadUint: " + bytecode[pos]);
				byte[] chunk = new byte[4];
				for (int i = 0; i < 4; i++)
				{
					chunk[i] = ReadByte();
				}
				uint uintCode = BitConverter.ToUInt32(chunk, 0);
				//this.pos+=4;
				return uintCode;
			}
			else
			{
				//PrintIfDebug("ReadUint called but found nothing.");
				this.pos = 0;
				return 0;
			}
		}

		uint PeekUint()
		{
			if (pos < bytecode.Length)
			{
				//PrintIfDebug("ReadUint: " + bytecode[pos]);
				byte[] chunk = new byte[4];
				for (int i = 0; i < 4; i++)
				{
					chunk[i] = bytecode[pos + i];
				}
				uint uintCode = BitConverter.ToUInt32(chunk, 0);
				return uintCode;
			}
			else
			{
				//PrintIfDebug("ReadUint called but found nothing.");
				this.pos = 0;
				return 0;
			}
		}

		uint ReadUint(ArrayList arr, int ip)
		{
			if (ip < arr.Count)
			{
				//PrintIfDebug("ReadUint: " + bytecode[pos]);
				uint chunk = bytecode[ip];
				ip++;
				return chunk;
			}
			else
			{
				//PrintIfDebug("ReadUint called but found nothing.");
				ip = 0;
				return 0;
			}
		}

		float ReadFloat()
		{
			if (pos < bytecode.Length)
			{
				//PrintIfDebug("ReadUint: " + bytecode[pos]);
				byte[] chunk = new byte[4];
				for (int i = 0; i < 4; i++)
				{
					chunk[i] = ReadByte();
				}
				float floatCode = BitConverter.ToSingle(chunk, 0);
				//this.pos+=4;
				return floatCode;
			}
			else
			{
				//PrintIfDebug("ReadFloat called but found nothing.");
				this.pos = 0;
				return 0;
			}
		}

		Int32 ReadInt()
		{
			if (pos < bytecode.Length)
			{
				//PrintIfDebug("ReadInt: " + bytecode[pos]);
				byte[] chunk = new byte[4];
				for (int i = 0; i < 4; i++)
				{
					chunk[i] = ReadByte();
				}
				int intCode = BitConverter.ToInt32(chunk, 0);
				//this.pos+=4;
				return intCode;
			}
			else
			{
				//PrintIfDebug("ReadUint called but found nothing.");
				this.pos = 0;
				return 0;
			}
		}

		public void interpret()
		{
			OpCode byteCode = (OpCode)ReadUint();
			//PrintIfDebug("Current bytecode is: " + byteCode);
			switch (byteCode)
			{
				case OpCode.OP_RET:
					PrintIfDebug("RET");
					this.pos = 0;
					return;
				case OpCode.OP_T:
					OpT();
					break;
				case OpCode.OP_NIL:
					OpNil();
					break;
				case OpCode.OP_CLEAR:
					OpClear();
					break;
				case OpCode.OP_PUSH_IMMEDIATE:
					OpPushImmediate();
					break;
				case OpCode.OP_CONST_STR:
					OpConstStr();
					break;
				case OpCode.OP_PUSH_FLOAT:
					OpPushFloat();
					break;
				case OpCode.OP_PUSH_DELTA_FLOAT:
					OpPushDeltaFloat();
					break;
				case OpCode.OP_PUSH_INT32:
					OpPushInt32();
					break;
				case OpCode.OP_OBJ:
					OpObj();
					break;
				case OpCode.OP_FUN:
					OpFun();
					break;
				case OpCode.OP_PUSH_STRING:
					OpPushString();
					break;
				case OpCode.OP_POP:
					OpPop();
					break;
				case OpCode.OP_REG_OBJ:
					OpRegObj();
					break;
				case OpCode.OP_REG_FUN:
					OpRegFun();
					break;
				case OpCode.OP_REG_CONST_STR:
					OpRegConstStr();
					break;
				case OpCode.OP_ADD:
					OpAdd();
					break;
				case OpCode.OP_SUB:
					OpSub();
					break;
				case OpCode.OP_MUL:
					OpMul();
					break;
				case OpCode.OP_DIV:
					OpDiv();
					break;
				case OpCode.OP_MOD:
					OpMod();
					break;
				case OpCode.OP_VEC:
					OpVec();
					break;
				case OpCode.OP_VADD:
					OpVAdd();
					break;
				case OpCode.OP_VSUB:
					OpVSub();
					break;
				case OpCode.OP_VCROSS:
					OpVCross();
					break;
				case OpCode.OP_VDOT:
					OpVDot();
					break;
				case OpCode.OP_VMUL_SC:
					OpVMulSc();
					break;
				case OpCode.OP_VDIV_SC:
					OpVDivSC();
					break;
				case OpCode.OP_GET:
					OpGet();
					break;
				case OpCode.OP_SET:
					OpSet();
					break;
				case OpCode.OP_EQ:
					OpEq();
					break;
				case OpCode.OP_NE:
					OpNE();
					break;
				case OpCode.OP_LESS:
					OpLess();
					break;
				case OpCode.OP_LE:
					OpLE();
					break;
				case OpCode.OP_GE:
					OpGE();
					break;
				case OpCode.OP_GREAT:
					OpGreat();
					break;
				case OpCode.OP_PROC:
					OpProc();
					break;
				case OpCode.OP_PHYS_PROC:
					OpPhysProc();
					break;
				case OpCode.OP_INPUT_PRESS:
					OpInputPress();
					break;
				case OpCode.OP_INPUT_JUST:
					OpInputJust();
					break;
				case OpCode.OP_ELSE:
					OpElse();
					break;
				case OpCode.OP_ELSE_INPUT:
					OpElseInput();
					break;
				case OpCode.OP_ENDIF:
					OpEndIf();
					break;
				case OpCode.OP_END_INPUT:
					OpEndInput();
					break;
				case OpCode.OP_INVOKE:
					OpInvoke();
					break;
				case OpCode.OP_INPUT_GET_MOUSEMODE:
					OpInputGetMouseMode();
					break;
				case OpCode.OP_INPUT_SET_MOUSEMODE:
					OpInputSetMouseMode();
					break;
				case OpCode.OP_PUSH_INPUT_EVENT:
					OpPushInputEvent();
					break;
				case OpCode.OP_PUSH_IE_MOUSEMOTION:
					OpPushIeMouseMotion();
					break;
				case OpCode.OP_EVENT:
					OpEvent();
					break;
				case OpCode.OP_TYPE:
					OpType();
					break;
				case OpCode.OP_SET_LOCAL:
					OpSetLocal();
					break;
				case OpCode.OP_GET_LOCAL:
					OpGetLocal();
					break;
				case OpCode.OP_ARRAY:
					OpArray();
					break;
				case OpCode.OP_NEW:
					OpNew();
					break;
				case OpCode.OP_META:
					OpMeta();
					break;
				case OpCode.OP_PUSH_RETURN:
					OpPushReturn();
					break;
				case OpCode.OP_PUSH_NULL:
					OpPushNull();
					break;
				case OpCode.OP_IS_NULL:
					OpIsNull();
					break;
				default:
					break;
			}
			interpret();
		}

		private void OpT()
		{
			PrintIfDebug("PUSH T");
			stack.Push(true);
		}

		private void OpNil()
		{
			PrintIfDebug("PUSH NIL");
			stack.Push(false);
		}

		private void OpClear()
		{
			PrintIfDebug("CLEAR");
			this.registeredProcesses = new ArrayList();
			this.registeredPhysProcesses = new ArrayList();
			this.registeredObjects = new Hashtable();
			this.registeredFunctions = new Hashtable();
			this.registeredConstants = new Hashtable();
			this.registeredEvents = new ArrayList();
			for (int i = 0; i < 256; i++)
			{
				this.registeredLocals[i] = 0;
			}
			this.stack.Clear();
			this.truthStack.Clear();
		}

		private void OpPushImmediate()
		{
			uint nextChunk = ReadUint();
			try
			{
				PrintIfDebug("PUSH_IMMEDIATE " + nextChunk);
				stack.Push(nextChunk);
			}
			catch (Exception e)
			{
				PrintIfDebug("Failed to push uint: " + nextChunk);
				PrintIfDebug(e);
			}
		}

		private void OpConstStr()
		{
			uint popUint = (uint)OpPop();
			try
			{
				PrintIfDebug("PUSH_CONST_STR " + popUint + "(" + registeredConstants[popUint] + ")");
				stack.Push(registeredConstants[popUint]);
			}
			catch (Exception e)
			{
				PrintIfDebug("Failed to push constant: " + popUint);
				PrintIfDebug(e);
			}
		}

		private void OpPushFloat()
		{
			float pushFloat = ReadFloat();
			PrintIfDebug("PUSH_FLOAT " + pushFloat);
			stack.Push(pushFloat);
		}

		private void OpPushDeltaFloat()
		{
			float pushFloat = ReadFloat() * dt;
			PrintIfDebug("PUSH_DELTA_FLOAT " + pushFloat);
			stack.Push(pushFloat);
		}

		private void OpPushInt32()
		{
			int pushInt = ReadInt();
			//pos += 4;
			PrintIfDebug("PUSH_INT32 " + pushInt);
			stack.Push(pushInt);
		}

		private void OpObj()
		{
			uint popUint = (uint)OpPop();
			try
			{
				PrintIfDebug("PUSH_OBJ " + popUint + "(" + registeredObjects[popUint] + ")");
				stack.Push(registeredObjects[popUint]);
			}
			catch (Exception e)
			{
				PrintIfDebug("Failed to push object: " + popUint);
				PrintIfDebug(e);
			}

		}

		private void OpFun()
		{
			uint popUint = (uint)OpPop();
			try
			{
				PrintIfDebug("PUSH_FUN " + popUint + "(" + registeredFunctions[popUint] + ")");
				stack.Push(registeredFunctions[popUint]);
			}
			catch (Exception e)
			{
				PrintIfDebug("Failed to push function: " + popUint);
				PrintIfDebug(e);
			}
		}

		private void OpPushString()
		{
			string pushed = "";
			uint nextUint = ReadUint();
			try
			{
				while ((OpCode)nextUint != OpCode.OP_END_STRING || !DoubleMetaPeek())
				{
					pushed += (char)nextUint;
					nextUint = ReadUint();
				}
				PrintIfDebug("PUSH_STRING " + pushed);
				stack.Push(pushed);
			}
			catch (Exception e)
			{
				PrintIfDebug("Failed to push string.");
				PrintIfDebug(e);
			}
		}

		object OpPop()
		{
			try {
				object popVal = stack.Pop();
				PrintIfDebug("POP " + popVal);
				//PrintTopOfStack();
				return popVal;
			}
			catch (Exception e)
			{
				PrintIfDebug("ERROR: POP NULL");
				PrintIfDebug(e);
				return null;
			}
		}

		private void OpRegObj()
		{
			uint popUint = (uint)OpPop();
			string objName = (string)OpPop();
			NodePath nodePath = new NodePath("/root/WORLD/" + objName);
			object popject = WORLD.root.GetNode<object>(nodePath);
			if (registeredObjects[popUint] == null)
			{
				registeredObjects.Add(popUint, popject);
				PrintIfDebug("REG_OBJ " + popUint + " " + popject);
			} else
			{
				PrintIfDebug("DUP REG_OBJ " + popUint + " " + registeredObjects[popUint] + " " + popject);
			}

		}

		private void OpRegFun()
		{
			uint registerUint = (uint)OpPop();
			string methodName = (string)OpPop();
			object popject = OpPop();
			Type type = popject.GetType();
			Int32 numParams = (Int32)OpPop();
			string stringTester = "";
			ArrayList paramTypeList = new ArrayList();

			// Static methods as functions will have their object be a string starting with S_ and ending with their type
			if (type == stringTester.GetType())
			{
				string popjectAsString = (string)popject;
				if (popjectAsString.StartsWith("S_")) {
					string staticType = popjectAsString.Substring(2);
					type = Type.GetType(staticType);
					PrintIfDebug("Static Type is assigned: ");
					PrintIfDebug(type);
				}
			}

			for (int i = 0; i < numParams; i++)
			{
				Type paramType = (Type)OpPop();
				paramTypeList.Add(paramType);
			}

			PrintIfDebug("Assigned Type is " + type.ToString());
			PrintIfDebug("Method name is " + methodName);
			PrintIfDebug(type.GetMethods().ToString());
			MethodInfo method;
			if (paramTypeList.Count == 0) {
				method = type.GetMethod(methodName, BindingFlags.FlattenHierarchy | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Public | BindingFlags.Instance);
			} else
			{
				method = type.GetMethod(methodName, BindingFlags.FlattenHierarchy | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Public | BindingFlags.Instance, Type.DefaultBinder, (Type[])paramTypeList.ToArray(type.GetType()), null);
			}

			if (registeredFunctions[registerUint] == null)
			{
				PrintIfDebug("REG-FUN " + registerUint + " " + method + " " + type);
				registeredFunctions.Add(registerUint, method);
			}
			else
			{
				PrintIfDebug("DUPLICATE REG-FUN " + registerUint + " " + method + " " + registeredFunctions[registerUint]);
			}
		}

		private void OpRegConstStr()
		{
			uint registerUint = (uint)OpPop();
			string con = (string)OpPop();
			if (registeredConstants[registerUint] == null)
			{
				PrintIfDebug("REG_CONST_STR " + registerUint + " " + con);
				registeredConstants.Add(registerUint, con);
			} else
			{
				PrintIfDebug("REG_CONST_STR " + registerUint + " " + registeredConstants[registerUint] + " " + con);
			}
		}

		void OpAdd()
		{
			float b = (float)OpPop();
			float a = (float)OpPop();
			PrintIfDebug("ADD " + a + " " + b);
			stack.Push(a + b);
		}

		void OpSub()
		{
			float b = (float)OpPop();
			float a = (float)OpPop();
			PrintIfDebug("SUB " + a + " " + b);
			stack.Push(a - b);
		}

		void OpMul()
		{
			float b = (float)OpPop();
			float a = (float)OpPop();
			PrintIfDebug("MUL " + a + " " + b);
			stack.Push(a * b);
		}

		void OpDiv()
		{
			float b = (float)OpPop();
			float a = (float)OpPop();
			PrintIfDebug("DIV " + a + " " + b);
			stack.Push(a / b);
		}

		void OpMod()
		{
			float b = (float)OpPop();
			float a = (float)OpPop();
			PrintIfDebug("MOD " + a + " " + b);
			stack.Push(a % b);
		}

		private void OpVec()
		{
			float c = (float)OpPop();
			float b = (float)OpPop();
			float a = (float)OpPop();
			PrintIfDebug("PUSH VEC " + "(" + a + ", " + b + ", " + c + ")");
			stack.Push(new Vector3(a, b, c));
		}

		private void OpVAdd()
		{
			Vector3 b = (Vector3)OpPop();
			Vector3 a = (Vector3)OpPop();
			PrintIfDebug("PUSH V3 ADD " + a + " " + b);
			stack.Push(a + b);
		}

		private void OpVSub()
		{
			Vector3 b = (Vector3)OpPop();
			Vector3 a = (Vector3)OpPop();
			PrintIfDebug("PUSH V3 SUB " + a + " " + b);
			stack.Push(a - b);
		}

		private void OpVCross()
		{
			Vector3 b = (Vector3)OpPop();
			Vector3 a = (Vector3)OpPop();
			PrintIfDebug("PUSH CROSS " + a + " " + b);
			stack.Push(a.Cross(b));
		}

		private void OpVDot()
		{
			Vector3 b = (Vector3)OpPop();
			Vector3 a = (Vector3)OpPop();
			PrintIfDebug("PUSH DOT " + a + " " + b);
			stack.Push(a.Dot(b));
		}

		private void OpVMulSc()
		{
			float scalar = (float)OpPop();
			Vector3 v3 = (Vector3)OpPop();
			PrintIfDebug("PUSH VEC*SCAL " + v3 + " " + scalar);
			stack.Push(v3 * scalar);
		}

		private void OpVDivSC()
		{
			float scalar = (float)OpPop();
			Vector3 v3 = (Vector3)OpPop();
			PrintIfDebug("PUSH VEC/SCAL " + v3 + " " + scalar);
			stack.Push(v3 / scalar);
		}

		private void OpGet()
		{
			string fieldOrProp = (string)OpPop();
			object popject = OpPop();
			Type objectType = popject.GetType();
			bool isStatic = false;
			if ("".GetType() == objectType)
			{
				objectType = Type.GetType(popject.ToString());
				isStatic = true;
			}
			PrintIfDebug("GET " + popject + " " + fieldOrProp);
			FieldInfo fieldInfo;
			PropertyInfo propertyInfo;
			try
			{

				fieldInfo = objectType.GetField(fieldOrProp);
				if (isStatic)
				{
					stack.Push(fieldInfo.GetValue(objectType));
				} else
				{
					stack.Push(fieldInfo.GetValue(popject));
				}
			}
			catch
			{
				propertyInfo = objectType.GetProperty(fieldOrProp);
				if (isStatic)
				{
					stack.Push(propertyInfo.GetValue(objectType));
				} else
				{
					stack.Push(propertyInfo.GetValue(popject));
				}
			}
			PrintIfDebug("PUSH " + stack.Peek());
		}

		private void OpSet()
		{
			object val = OpPop();
			string fieldOrProp = (string)OpPop();
			object popject = OpPop();

			Type objectType = popject.GetType();
			bool isStatic = false;
			if ("".GetType() == objectType)
			{
				objectType = Type.GetType(popject.ToString());
				isStatic = true;
			}

			FieldInfo fieldInfo;
			PropertyInfo propertyInfo;
			try
			{
				fieldInfo = objectType.GetField(fieldOrProp);
				if (isStatic)
				{
					fieldInfo.SetValue(objectType, val);
				} else
				{
					fieldInfo.SetValue(popject, val);
					stack.Push(popject);
				}

				PrintIfDebug("SET " + fieldInfo + " " + popject + " " + val);

				PrintIfDebug("PUSH " + popject);
			}
			catch
			{
				propertyInfo = objectType.GetProperty(fieldOrProp);
				if (isStatic)
				{
					propertyInfo.SetValue(objectType, val);
				}
				else
				{
					propertyInfo.SetValue(popject, val);
					stack.Push(popject);
				}
				PrintIfDebug("SET " + propertyInfo + " " + popject + " " + val);

				PrintIfDebug("PUSH " + popject);
			}
		}

		private void OpEq()
		{
			object b = OpPop();
			object a = OpPop();
			PrintIfDebug("EQ " + a + " " + b);
			if (a.Equals(b))
			{
				PrintIfDebug("TRUE");
				truthStack.Push(true);
			}
			else
			{
				PrintIfDebug("FALSE");
				truthStack.Push(false);
				JumpToElseOrEndIf();
			}
		}

		private void OpNE()
		{
			PrintIfDebug("Begin NE, OpCode: " + OpCode.OP_NE);
			object b = OpPop();
			object a = OpPop();
			PrintIfDebug("NE " + a + " " + b);
			if (!a.Equals(b))
			{
				truthStack.Push(true);
			}
			else
			{
				truthStack.Push(false);
				JumpToElseOrEndIf();
			}
		}

		private void OpLess()
		{
			object b = OpPop();
			object a = OpPop();
			PrintIfDebug("LESS " + a + " " + b);
			if ((float)a < (float)b)
			{
				truthStack.Push(true);
			}
			else
			{
				truthStack.Push(false);
				JumpToElseOrEndIf();
			}
		}

		private void OpLE()
		{
			object b = OpPop();
			object a = OpPop();
			PrintIfDebug("LE " + a + " " + b);
			if ((float)a <= (float)b)
			{
				truthStack.Push(true);
			}
			else
			{
				truthStack.Push(false);
				JumpToElseOrEndIf();
			}
		}

		private void OpGE()
		{
			object b = OpPop();
			object a = OpPop();
			PrintIfDebug("GE " + a + " " + b);
			if ((float)a >= (float)b)
			{
				truthStack.Push(true);
			}
			else
			{
				truthStack.Push(false);
				JumpToElseOrEndIf();
			}
		}

		private void OpGreat()
		{
			object b = OpPop();
			object a = OpPop();
			PrintIfDebug("GREAT " + a + " " + b);
			if ((float)a > (float)b)
			{
				truthStack.Push(true);
			}
			else
			{
				truthStack.Push(false);
				JumpToElseOrEndIf();
			}
		}

		private void OpProc()
		{
			uint currentUint = PeekUint();
			ArrayList proc = new ArrayList();
			while ((OpCode)currentUint != OpCode.OP_END_PROC || !SkipDoubleMetaPeek())
			{
				for (int i = 0; i < 4; i++)
				{
					proc.Add(ReadByte());
				}
				currentUint = PeekUint();
			}
			ReadUint();
			registeredProcesses.Add(proc);
		}

		private void OpPhysProc()
		{
			uint currentUint = PeekUint();
			ArrayList proc = new ArrayList();
			while ((OpCode)currentUint != OpCode.OP_END_PROC || !SkipDoubleMetaPeek())
			{
				for (int i = 0; i < 4; i++)
				{
					proc.Add(ReadByte());
				}
				currentUint = PeekUint();
			}
			ReadUint();
			registeredPhysProcesses.Add(proc);
		}

		private void OpInputPress()
		{
			string action = (string)OpPop();
			PrintIfDebug("INPUT_PRESS " + action);
			if (Input.IsActionPressed(action))
			{
				inputTruthStack.Push(true);
			}
			else
			{
				inputTruthStack.Push(false);
				InputJumpToElseOrEndIf();
			}
		}

		private void OpInputJust()
		{
			string action = (string)OpPop();
			PrintIfDebug("INPUT_JUST " + action);
			if (Input.IsActionJustPressed(action))
			{
				PrintIfDebug("Action is just pressed: " + action);
				inputTruthStack.Push(true);
			}
			else
			{
				PrintIfDebug("Action is NOT just pressed: " + action);
				inputTruthStack.Push(false);
				InputJumpToElseOrEndIf();
			}
		}

		private void JumpToElseOrEndIf()
		{
			uint currentUint = ReadUint();
			while ((currentUint != (uint)OpCode.OP_ENDIF && currentUint != (uint)OpCode.OP_ELSE) || !DoubleMetaPeek())
			{
				currentUint = ReadUint();
			}

			if ((OpCode)currentUint == OpCode.OP_ENDIF)
			{
				if (DoubleMetaPeek()) {
					truthStack.Pop();
				}
			}
		}


		private void InputJumpToElseOrEndIf()
		{
			uint currentUint = ReadUint();
			while ((currentUint != (uint)OpCode.OP_END_INPUT && currentUint != (uint)OpCode.OP_ELSE_INPUT) || !DoubleMetaPeek())
			{
				currentUint = ReadUint();
			}

			if ((OpCode)currentUint == OpCode.OP_END_INPUT)
			{
				if (DoubleMetaPeek())
				{
					inputTruthStack.Pop();
				}
			}
		}

		private void OpEndIf()
		{
			PrintIfDebug("ENDIF");
			if (DoubleMetaPeek() && truthStack.Count != 0)
			{
				truthStack.Pop();
			}
		}

		private void OpEndInput()
		{
			PrintIfDebug("END_INPUT");
			if (DoubleMetaPeek() && inputTruthStack.Count != 0)
			{
				inputTruthStack.Pop();
			}
		}


		private void OpElse()
		{
			if (DoubleMetaPeek() && (Boolean)truthStack.Pop())
			{
				uint currentUint = ReadUint();
				while ((OpCode)currentUint != OpCode.OP_ENDIF || !DoubleMetaPeek())
				{
					currentUint = ReadUint();
				}
			}
		}

		private void OpElseInput()
		{
			if (DoubleMetaPeek() && (Boolean)truthStack.Pop())
			{
				uint currentUint = ReadUint();
				while ((OpCode)currentUint != OpCode.OP_END_INPUT || !DoubleMetaPeek())
				{
					currentUint = ReadUint();
				}
			}
		}

		private void OpInvoke()
		{
			try
			{
				MethodInfo invokedOp = (MethodInfo)OpPop();
				object instance = null;
				object returnVal = null;
				Int32 numParams = invokedOp.GetParameters().Length;
				if (!invokedOp.IsStatic)
				{
					instance = OpPop();
					PrintIfDebug("INSTANCE " + instance);
				}
				//PrintIfDebug("INVOKE ");
				ArrayList args = new ArrayList();
				for (int i = numParams; i > 0; i--)
				{
					object poppedArg = OpPop();
					if (poppedArg != null)
					{
						args.Add(poppedArg);
					}
				}
				if (args.Count == 0)
				{
					PrintIfDebug("INVOKE " + invokedOp + " " + instance);
					returnVal = invokedOp.Invoke(instance, null);
					if (returnVal != null)
					{
						this.returnRegister = returnVal;
					}
				}
				else
				{
					object[] argArr = args.ToArray();
					PrintIfDebug("INVOKE " + invokedOp + " " + instance + " " + argArr);
					returnVal = invokedOp.Invoke(instance, argArr);
					if (returnVal != null)
					{
						this.returnRegister = returnVal;
					}
				}
			} catch (Exception e)
			{
				PrintIfDebug("ERROR: Unable to invoke method: ");
				PrintIfDebug(e);
			}
		}

		private void OpInputGetMouseMode()
		{
			stack.Push(Input.GetMouseMode().ToString());
		}

		private void OpInputSetMouseMode()
		{
			// 0 is visible
			// 1 is hidden
			// 2 is captured
			// 3 is confined
			Godot.Input.MouseMode mode = (Godot.Input.MouseMode)stack.Pop();
			Input.SetMouseMode(mode);
		}

		private void OpPushInputEvent()
		{
			stack.Push(@event);
		}

		private void OpPushIeMouseMotion()
		{
			InputEventMouseMotion ev = new InputEventMouseMotion();
			stack.Push(ev.GetType());
		}

		private void OpEvent()
		{
			uint currentUint = PeekUint();
			ArrayList ev = new ArrayList();
			while ((OpCode)currentUint != OpCode.OP_END_EVENT || !SkipDoubleMetaPeek())
			{
				for (int i = 0; i < 4; i++)
				{
					ev.Add(ReadByte());
				}
				currentUint = PeekUint();
			}
			ReadUint();
			registeredEvents.Add(ev);
		}

		private void OpType()
		{
			object popject = stack.Pop();
			if (popject.GetType() == "".GetType())
			{
				PrintIfDebug("TYPE " + Type.GetType(popject.ToString()));
				stack.Push(Type.GetType(popject.ToString()));
			} else {
				PrintIfDebug("TYPE " + popject.GetType());
				stack.Push(popject.GetType());
			}
		}

		private void OpSetLocal()
		{
			uint popUint = (uint)OpPop();
			object popject = OpPop();
			try
			{
				object shallowCopy = new object();
				shallowCopy = popject;
				registeredLocals[popUint] = shallowCopy;
				PrintIfDebug("SET_LOCAL " + popUint + " ( " + registeredLocals[popUint] + " ) ");
			}
			catch (Exception e)
			{
				PrintIfDebug("Failed to set local: " + popUint);
				PrintIfDebug(e);
			}
		}

		private void OpGetLocal()
		{
			uint popUint = (uint)OpPop();
			try
			{
				PrintIfDebug("GET_LOCAL " + popUint + " ( " + registeredLocals[popUint] + " ) ");
				stack.Push(registeredLocals[popUint]);
			}
			catch (Exception e)
			{
				PrintIfDebug("Failed to push local: " + popUint);
				PrintIfDebug(e);
			}
		}

		private void OpArray()
		{
			Int32 popInt = (Int32)OpPop();
			ArrayList arrList = new ArrayList();
			for(int i = 0; i < popInt; i++)
			{
				arrList.Add(OpPop());
			}
			stack.Push(arrList.ToArray());

		}

		private void OpNew()
		{
			// Ensure that the string being passed has appropriate assembly naming convention
			// Ex. Godot.GD, GodotSharp
			string typeAsString = (string)OpPop();
			Int32 nargs = (Int32)OpPop();
			PrintIfDebug("NEW " + typeAsString);
			try
			{
				object new_obj;
				if(nargs == 0)
				{
					new_obj = Activator.CreateInstance(Type.GetType(typeAsString));
				} else
				{
					ArrayList args = new ArrayList();
					for(int i = 0; i < nargs; i++)
					{
						object arg = OpPop();
						args.Add(arg);
					}
					new_obj = Activator.CreateInstance(Type.GetType(typeAsString), args.ToArray());
				}
				PrintIfDebug("PUSH " + new_obj.GetType());
				stack.Push(new_obj);
			} catch (Exception e)
			{
				PrintIfDebug("Failed to create new " + typeAsString);
				PrintIfDebug(e);
			}
		}

		private void OpMeta()
		{
			PrintIfDebug("META");
		}

		private Boolean DoubleMetaPeek()
		{
			// Used for end of string/input or any other seek style bytecodes
			PrintIfDebug("SEEK DOUBLE META");
			if (pos < bytecode.Length - 8)
			{
				//PrintIfDebug("ReadUint: " + bytecode[pos]);
				byte[] chunk = new byte[8];
				for (int i = 0; i < 8; i++)
				{
					chunk[i] = bytecode[pos + i];
				}
				uint peekUint1 = BitConverter.ToUInt32(chunk, 0);
				uint peekUint2 = BitConverter.ToUInt32(chunk, 4);
				return (peekUint1 == (uint)OpCode.OP_META && peekUint2 == (uint)OpCode.OP_META);
			}
			else
			{
				return true;
			}
		}

		private Boolean SkipDoubleMetaPeek()
		{
			// Used for end of string/input or any other seek style bytecodes
			PrintIfDebug("SEEK DOUBLE META");
			if (pos < bytecode.Length - 12)
			{
				//PrintIfDebug("ReadUint: " + bytecode[pos]);
				byte[] chunk = new byte[8];
				for (int i = 4; i < 12; i++)
				{
					chunk[i - 4] = bytecode[pos + i];
				}
				uint peekUint1 = BitConverter.ToUInt32(chunk, 0);
				uint peekUint2 = BitConverter.ToUInt32(chunk, 4);
				PrintIfDebug("uint1 " + (OpCode)peekUint1);
				PrintIfDebug("uint2 " + (OpCode)peekUint2);
				PrintIfDebug("double meta true? " + (peekUint1 == (uint)OpCode.OP_META && peekUint2 == (uint)OpCode.OP_META));
				return (peekUint1 == (uint)OpCode.OP_META && peekUint2 == (uint)OpCode.OP_META);
			}
			else
			{
				return true;
			}
		}

		void OpPushReturn()
		{
			PrintIfDebug("PUSH_RETURN_VAL" + returnRegister);
			stack.Push(returnRegister);
		}

		void OpPushNull()
		{
			PrintIfDebug("PUSH NULL");
			stack.Push(null);
		}

		private void OpIsNull()
		{
			int a = (int)OpPop();
			PrintIfDebug("a" + " IS NULL?");
			if (registeredLocals[a] is null)
			{
				PrintIfDebug("TRUE");
				truthStack.Push(true);
			}
			else
			{
				PrintIfDebug("FALSE");
				truthStack.Push(false);
				JumpToElseOrEndIf();
			}
		}

		private void PrintIfDebug(object msg)
		{
			if (debug)
			{
				System.Console.WriteLine(msg);
			}
		}

		void ReplIt()
		{
			byte[] replArray = new byte[ARRAY_SIZE];
			this.preludePbuf.ReadArray(0, replArray, 0, (int)ARRAY_SIZE);
			this.bytecode = replArray;
			interpret();
			replArray = new byte[ARRAY_SIZE];
			this.pbuf.ReadArray(4, replArray, 0, (int)ARRAY_SIZE);
			this.pos = 0;
			this.bytecode = replArray;
			PrintIfDebug(this.bytecode[0]);
			interpret();
		}

		void TestFun()
		{
			MethodInfo mi = typeof(Vector2).GetMethod("op_Addition");
			Vector2 vvec = new Vector2();
			Vector2 vvvec = vvec + vvec;
			Vector3 vec3test = new Vector3();
			Camera camera = new Camera();
			Spatial spatial = new Spatial();
			KinematicBody kb = new KinematicBody();
			
		}

		void LoadLispByteCodeFromFile()
		{
			//File res = (File)ResourceLoader.Load("res://crash.fasb", "File");
			File byteCodeFile = new File();
			try
			{
				byteCodeFile.Open("res://crash.fasb", File.ModeFlags.Read);
				byte[] fileByteArray = byteCodeFile.GetBuffer((int)byteCodeFile.GetLen());
				for (int i = 0; i < fileByteArray.Length; i++)
				{
					this.bytecode[i] = fileByteArray[i];
				}
			} catch (Exception e)
			{
				Console.WriteLine(e);
			}
			finally
			{
				byteCodeFile.Close();
				this.fileLoaded = true;
			}
		}

		public void _Input(InputEvent @event)
		{
			//base._Input(@event);
			this.@event = @event;
			interpret();
		}

		public void WatchRepl()
		{
			if (this.pbuf.ReadUInt32(0) == 1)
			{
				this.pbuf.Write(0, (uint)0);
				ReplIt();
			}
		}
	}
}
