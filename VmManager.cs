﻿using Godot;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crash
{
    class VmManager
    {
        public LispVM vm;
        public ArrayList processVMList;
        public ArrayList physProcessVMList;
        public ArrayList eventVMList;

        public VmManager()
        {
            this.vm = new LispVM();
            this.processVMList = new ArrayList();
            this.physProcessVMList = new ArrayList();
            this.eventVMList = new ArrayList();
        }

        public void RunProcs(float dt)
        {
            if (this.vm.registeredProcesses.Count != this.processVMList.Count)
            {

                this.processVMList = new ArrayList();
                foreach (ArrayList arr in this.vm.registeredProcesses)
                {
                    LispVM newVM = new LispVM(arr);
                    newVM.registeredObjects = this.vm.registeredObjects;
                    newVM.registeredFunctions = this.vm.registeredFunctions;
                    newVM.registeredConstants = this.vm.registeredConstants;
                    this.processVMList.Add(newVM);

                }
            }
            foreach (LispVM procVM in this.processVMList)
            {
                procVM.dt = dt;
                procVM.interpret();
            }
        }

        public void RunPhysProcs(float dt)
        {
            if (this.vm.registeredPhysProcesses.Count != this.physProcessVMList.Count)
            {

                this.physProcessVMList = new ArrayList();
                foreach (ArrayList arr in this.vm.registeredPhysProcesses)
                {
                    LispVM newVM = new LispVM(arr);
                    newVM.registeredObjects = this.vm.registeredObjects;
                    newVM.registeredFunctions = this.vm.registeredFunctions;
                    newVM.registeredConstants = this.vm.registeredConstants;
                    this.physProcessVMList.Add(newVM);

                }
            }
            foreach (LispVM physVM in this.physProcessVMList)
            {
                physVM.dt = dt;
                physVM.interpret();
            }
        }

        public void RunEventProcs(float dt)
        {
            if (this.vm.registeredEvents.Count != this.eventVMList.Count)
            {

                this.eventVMList = new ArrayList();
                foreach (ArrayList arr in this.vm.registeredEvents)
                {
                    LispVM newVM = new LispVM(arr);
                    newVM.registeredObjects = this.vm.registeredObjects;
                    newVM.registeredFunctions = this.vm.registeredFunctions;
                    newVM.registeredConstants = this.vm.registeredConstants;
                    this.eventVMList.Add(newVM);

                }
            }
            foreach (LispVM eventVM in this.eventVMList)
            {
                eventVM.dt = dt;
            }
        }

        public void RunEventsOnEvent(InputEvent @event)
        {
            foreach (LispVM eventVM in this.eventVMList)
            {
                eventVM._Input(@event);
            }
        }
    }
}
