(defsystem "crash"
  :version "0.1.0"
  :author "Jason Chandler"
  :license "Personal"
  :depends-on ()
  :components ((:module "src"
                :components
			((:file "file-map")
			 (:file "operations")
			 (:file "repl")
			 (:file "model")
			 (:file "high")
			 (:file "main")
			 (:file "compile"))))
  :description "For use with CL vm for Godot")
