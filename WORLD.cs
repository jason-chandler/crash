using Crash;
using Godot;
using System;

public class WORLD : Spatial
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";
	public static Node root;
	VmManager vmManager = new VmManager();

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		if (root == null)
		{
			root = this.GetTree().Root;
		}
		if (!LispVM.devMode)
		{
			while(!vmManager.vm.fileLoaded)
			{

			}
			vmManager.vm.interpret();
		}
	}

	public override void _Process(float delta)
	{
		if (LispVM.devMode)
		{
			vmManager.vm.WatchRepl();
		} 
		vmManager.RunProcs(delta);
		vmManager.RunEventProcs(delta);
	}

	public override void _PhysicsProcess(float delta)
	{
		base._PhysicsProcess(delta);
		vmManager.RunPhysProcs(delta);
	}

	public override void _Input(InputEvent @event)
	{
		base._Input(@event);
		vmManager.RunEventsOnEvent(@event);
	}

}
